struct RecogParams
{
	int blackValue, redValue, redDiff;
	int colInterval, rowInterval;
	int recogbound, recogendbound;
};

//
int recognizeImage(const QPixmap &img, const RecogParams &params);