#include "ffmpegWorker.h"

#include <stdio.h>
#include "log4cplus.h"
#include "ProgramConfig.h"
#include "devices.h"
#include <QFileInfo>

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif
#include <libavcodec\avcodec.h>
#include <libavformat\avformat.h>
#include <libswscale\swscale.h>
#include <libavutil\avutil.h>
}

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(55,28,1)
#define av_frame_alloc avcodec_alloc_frame
#define av_frame_free avcodec_free_frame
#endif


//
void avlogcallback(void *hmm, int loglevel, const char *str, va_list lst)
{
	if (loglevel > AV_LOG_DEBUG)
		return;
	
	static log4cplus::Logger log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("FFMpeg_log"));

	static int accsize = 0;
	static char buff[2048];
	int inlen = strlen(str);

	int n = vsprintf_s(buff + accsize, 2048, str, lst);

	if (buff[accsize + n - 1] == '\n'){
		for (int i = 0; i < accsize + n - 1; i++)
			if (buff[i] == '\r')
				buff[i] = ' ';
		buff[accsize + n - 1] = '\0';
		accsize = 0;
		if (loglevel < AV_LOG_ERROR)
			LOG4CPLUS_ERROR(log, buff);
		else if (loglevel < AV_LOG_WARNING)
			LOG4CPLUS_WARN(log, buff);
		else
			LOG4CPLUS_DEBUG(log, buff);
	}
	else
		accsize += n;
}

int FFMPEG_TIMEOUT = 5000;
QElapsedTimer ELAPSEDTIMER;
//
int interrupt_cb(void *id)
{
	return ELAPSEDTIMER.elapsed() > FFMPEG_TIMEOUT;
}

//
void initFFMPEG()
{
	static bool initialized = false;
	if (initialized) return;

	av_log_set_level(AV_LOG_DEBUG);
	av_register_all();
	avcodec_register_all();
	avformat_network_init();

	av_log_set_callback(&avlogcallback);
}

////////////////////////////////////////////////////////
// TFFMpegWorker ///////////////////////////////////////
////////////////////////////////////////////////////////
//
TFFMpegWorker::TFFMpegWorker()
	: m_record(false)
	, m_finish(false)
	, m_snapshot(false)
	, m_inFormatCtx(NULL)
	, m_goingtostart(false)
{
	m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("FFMpeg_worker"));
	initFFMPEG();

	m_permRecCtx.validOut = false;
	m_OrdRecCtx.validOut = false;
}

//
QString permName( const QString &path )
{
	auto cfg = ProgramConfig::instance().capture();
	return QString("%1/%2.%3").arg(path).arg(QDateTime::currentDateTime().toString("yyyy.MM.dd-hh.mm")).arg(cfg->videoExt);
}

//
void TFFMpegWorker::setDeviceInfo(const DeviceInfo &devinfo)
{
	m_devInfo = devinfo;

	m_keyInterval = m_devInfo.frameInterval * 1000;

	auto cfg = ProgramConfig::instance().capture();
	m_permRecCtx.file = permName(m_devInfo.permanentRecordPath);
}

//
void TFFMpegWorker::slotStart(QString url, int w, int h)
{
	emit started();

	m_finish = false;
	m_url = url;
	qDebug() << "Worker started.";

	if (!initSource()){
		emit prepared(false);
		emit stopped();
		return;
	}

	auto pCodec = avcodec_find_decoder(m_inCodecCtx->codec_id);
	m_codecCtx = avcodec_alloc_context3(pCodec);
	avcodec_copy_context(m_codecCtx, m_inCodecCtx);
	avcodec_open2(m_codecCtx, pCodec, NULL);

	m_previewWidth = m_inCodecCtx->width;
	m_previewHeight = m_inCodecCtx->height;

	m_swsPreviewCtx = sws_getContext(
		m_inCodecCtx->width, m_inCodecCtx->height
		, m_inCodecCtx->pix_fmt
		, m_previewWidth, m_previewHeight
		, AV_PIX_FMT_RGB24, SWS_BILINEAR, NULL, NULL, NULL );

	prepareFramesInfo();

	int numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, m_previewWidth, m_previewHeight);
	
	AVPacket pkt, pkt2;
	av_init_packet(&pkt);
	av_init_packet(&pkt2);
	auto freePackets = [&] {
		av_free_packet(&pkt);
		av_free_packet(&pkt2);
		av_init_packet(&pkt);
		av_init_packet(&pkt2);
	};

	int i = 0;

	emit prepared(true);

	FFMPEG_TIMEOUT = 2000;
	ELAPSEDTIMER.start();
	QElapsedTimer keyframe_timer;
	keyframe_timer.start();

	auto cfg = ProgramConfig::instance().capture();
	if (m_devInfo.permanentRecord) {
		m_permTimer.reset(new QTimer);
		m_permTimer->setInterval(m_devInfo.permRecMin * 60 * 1000);
		connect(m_permTimer.get(), &QTimer::timeout, this, [this]() {
			endRecord(m_permRecCtx, false);
			m_permRecCtx.file = permName(m_devInfo.permanentRecordPath);
			beginRecord(m_permRecCtx, false);
		}); 
		beginRecord(m_permRecCtx, false);
		m_permTimer->start();
	}
	
	while (!m_finish && av_read_frame(m_inFormatCtx, &pkt) >= 0) {

		if (++i % 8) {
			QEventLoop loop;
			loop.processEvents();
			ELAPSEDTIMER.start();
		}

		if (pkt.stream_index == m_videoIdx) {
			static bool keyframe = false;

			if (m_goingtostart) {
				if (pkt.flags & AV_PKT_FLAG_KEY)
					m_goingtostart = false;
				else {
					freePackets();
					continue;
				}
			}
			
			if ((pkt.flags & AV_PKT_FLAG_KEY)
				&& ((keyframe_timer.elapsed() > m_keyInterval) || m_snapshot)) {
				keyframe = true;
			}
			

			if( !cfg->qtavwidget && (keyframe || m_keyInterval == 0)){
				int frameFinished = 0;
				int nres = avcodec_decode_video2(m_codecCtx, m_frame, &frameFinished, &pkt);
				if (frameFinished) {
					if (m_frame->key_frame || m_keyInterval == 0) {
						nres = sws_scale(m_swsPreviewCtx, (uint8_t const * const *)m_frame->data
							, m_frame->linesize, 0, m_codecCtx->height
							, m_frameRGB->data, m_frameRGB->linesize);

						char *buff = new char[numBytes];
						memcpy(buff, m_frameRGB->data[0], numBytes);
						emit image((char*)buff, m_previewWidth, m_previewHeight);

						if (m_snapshot)
							makeSnapshot(m_frame);

						keyframe = false;
						keyframe_timer.restart();
					}
				}
			}

			auto writePacket = [] (CtxStreamInfo &streamInfo, AVPacket &packet){
				packet.pts = streamInfo.pts++;
				packet.dts = packet.pts;
				packet.duration = av_rescale_q(packet.duration, streamInfo.stream->codec->time_base, streamInfo.stream->time_base) /*/ 3600*/;
				packet.stream_index = streamInfo.stream->id;
				av_interleaved_write_frame(streamInfo.ctx, &packet);
			};

			if (m_record && m_OrdRecCtx.validOut ) {
				av_copy_packet(&pkt2, &pkt);
				av_copy_packet_side_data(&pkt2, &pkt);
				writePacket(m_OrdRecCtx, pkt2);
			}
			if (m_devInfo.permanentRecord && m_permRecCtx.validOut) {
				writePacket(m_permRecCtx, pkt);
			}
		}

		freePackets();
	}
	
	av_free_packet(&pkt2);
	av_free_packet(&pkt);

	if (m_record) {
		LOG4CPLUS_FATAL(m_log, QString("slotStart+ ORD rec end").toStdString().c_str());
		endRecord(m_OrdRecCtx);
		m_record = false;
	}
	if (m_devInfo.permanentRecord) {
		LOG4CPLUS_FATAL(m_log, QString("slotStart+ PERM rec end").toStdString().c_str());
		endRecord(m_permRecCtx, false);
		m_permTimer->stop();
	}

	avformat_close_input(&m_inFormatCtx);
	avcodec_close(m_codecCtx);
	sws_freeContext(m_swsPreviewCtx);
	av_frame_free(&m_frame);
	av_frame_free(&m_frameRGB);
	av_free(m_buffer);

	avformat_network_deinit();
	emit prepared(false);
	emit stopped();
}

//
bool TFFMpegWorker::initSource()
{
	AVDictionary *opts = NULL;
	if( Encoders::encoderInfo(m_devInfo.stdSourceId).useTcpTransport )
		av_dict_set(&opts, "rtsp_transport", "tcp", 1);

	m_inFormatCtx = avformat_alloc_context();
	m_inFormatCtx->interrupt_callback.callback = interrupt_cb;
	m_inFormatCtx->interrupt_callback.opaque = (void*)1;

	FFMPEG_TIMEOUT = 5000;
	ELAPSEDTIMER.start();
	int res = avformat_open_input(&m_inFormatCtx, m_url.toLatin1(), NULL, &opts);
	emit connected(res==0);
	if ( res != 0) {
		LOG4CPLUS_FATAL(m_log, QString("avformat_open_input failed: %1").arg(m_url).toStdString().c_str());
		qDebug() << QString("avformat_open_input failed: %1").arg(m_url);
		emit findStreamFail();
		return false;
	}

	m_inFormatCtx->interrupt_callback.callback = NULL;
	m_inFormatCtx->interrupt_callback.opaque = NULL;
	ELAPSEDTIMER.start();
	if (avformat_find_stream_info(m_inFormatCtx, NULL) < 0) {
		avformat_close_input(&m_inFormatCtx);
		LOG4CPLUS_FATAL(m_log, QString("avformat_find_stream_info failed: %1").arg(m_url).toStdString().c_str());
		qDebug() << QString("avformat_find_stream_info failed: %1").arg(m_url);
		emit findStreamFail();
		return false;
	}
	
	//search video stream
	m_videoIdx = -1;
	for (uint ix = 0; ix < m_inFormatCtx->nb_streams; ix++) {
		m_inCodecCtx = m_inFormatCtx->streams[ix]->codec;
		if (m_inCodecCtx->codec_type == AVMEDIA_TYPE_VIDEO) {
			m_inStream = m_inFormatCtx->streams[ix];
			m_videoIdx = ix;
			break;
		}
	}

	if (m_inStream->codec->width == 0 || m_inStream->codec->height == 0) {
		qDebug() << "Input stream width-height == 0";
		LOG4CPLUS_FATAL(m_log, QString("Input stream width-height == 0").toStdString().c_str());
		avformat_close_input(&m_inFormatCtx);
		emit findStreamFail();
		return false;
	}

	if (m_videoIdx < 0) {
		avformat_close_input(&m_inFormatCtx);
		qDebug() << "EXIT_FAILURE: close input";
		emit findStreamFail();
		return false;
	}

	LOG4CPLUS_FATAL(m_log, QString("AVG: %1/%2, R_: %3/%4")
		.arg(m_inStream->avg_frame_rate.num).arg(m_inStream->avg_frame_rate.den)
		.arg(m_inStream->r_frame_rate.num).arg(m_inStream->r_frame_rate.den)
		.toStdString().c_str());
	if (m_inStream->avg_frame_rate.num == 0) {
		if(m_inStream->avg_frame_rate.num == 0 && m_devInfo.correctFrameRate){
			LOG4CPLUS_FATAL(m_log, QString("Correcting framerate to 25").toStdString().c_str());
			m_inStream->r_frame_rate.num = m_inStream->avg_frame_rate.num = m_devInfo.frameRateValue;
			m_inStream->r_frame_rate.den = m_inStream->avg_frame_rate.den = 1;
		}
		else
			LOG4CPLUS_FATAL(m_log, QString("m_inStream->avg_frame_rate.num == 0 && m_devInfo.correctFrameRate = false").toStdString().c_str());
	}

	av_dump_format(m_inFormatCtx, 0, m_url.toLatin1(), 0);
	return true;
}

//
void TFFMpegWorker::prepareFramesInfo()
{
	m_frame = av_frame_alloc();
	m_frameRGB = av_frame_alloc();

	// Determine required buffer size and allocate buffer
	int numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, m_previewWidth, m_previewHeight);
	m_buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));
	avpicture_fill((AVPicture *)m_frameRGB, m_buffer, AV_PIX_FMT_RGB24, m_previewWidth, m_previewHeight);
}

//
void TFFMpegWorker::beginRecord( CtxStreamInfo &outCtx, bool ordRecord )
{
	LOG4CPLUS_INFO(m_log, QString("beginRecord %1").arg(outCtx.file).toStdString().c_str());
	qDebug() << "begin " << outCtx.file;
	
	outCtx.validOut = false;
	auto cfg = ProgramConfig::instance().capture();
	QString ext = cfg->videoExt;
	int i = 0;
	while (QFile::exists(outCtx.file)){
		QString orFN = outCtx.file.left(outCtx.file.length() - ext.length() - 1);
		outCtx.file = orFN + "-." + ext;
		i++;
		if (i > 10) {
			qDebug() << "Tried " << outCtx.file << "few times unsuccessfully.";
			LOG4CPLUS_INFO(m_log, QString("beginRecord: Tried %1 %2 times unsuccessfully.")
				.arg(outCtx.file).arg(i).toStdString().c_str());
			return;
		}
	}
	
	if (m_devInfo.permanentRecord) {
		if (!ordRecord)
			m_mutex.lock();
	}
	else
		m_mutex.lock();

	outCtx.validOut = true;
	
	AVOutputFormat *ofmt;
	ofmt = av_guess_format(NULL, outCtx.file.toLatin1(), NULL);

	outCtx.ctx = avformat_alloc_context();
	outCtx.ctx->oformat = ofmt;

	avio_open2(&outCtx.ctx->pb, outCtx.file.toLatin1(), AVIO_FLAG_WRITE, NULL, NULL);
	
	// Create output stream
	outCtx.stream = avformat_new_stream(outCtx.ctx, NULL);
	avcodec_copy_context(outCtx.stream->codec, m_inCodecCtx);

	outCtx.stream->sample_aspect_ratio.num = m_inCodecCtx->sample_aspect_ratio.num;
	outCtx.stream->sample_aspect_ratio.den = m_inCodecCtx->sample_aspect_ratio.den;

	// Assume r_frame_rate is accurate
	outCtx.stream->r_frame_rate = m_inStream->r_frame_rate;
	outCtx.stream->avg_frame_rate = outCtx.stream->r_frame_rate;
	outCtx.stream->time_base = av_inv_q(outCtx.stream->r_frame_rate);
	//outCtx.stream->codec->time_base = outCtx.stream->time_base;
	outCtx.stream->codec->time_base = m_inStream->time_base;

	avformat_write_header(outCtx.ctx, NULL);

	outCtx.ctx->streams[0]->start_time = 0;

	sprintf_s(outCtx.ctx->filename, 1024, "%s", outCtx.file.toLatin1().constData());

	outCtx.pts = 0;
	av_dump_format(outCtx.ctx, 0, outCtx.ctx->filename, 1);

	if(ordRecord) emit startRecord();

	m_goingtostart = true;
}

//
void TFFMpegWorker::endRecord(CtxStreamInfo &outCtx, bool ordRecord)
{

	if (!outCtx.validOut) {
		qDebug() << "outCtx.validOut " << outCtx.validOut;
		LOG4CPLUS_INFO(m_log, QString("outCtx.validOut is invalid").toStdString().c_str());
		return;
	}

	qDebug() << "end " << outCtx.file;
	LOG4CPLUS_INFO(m_log, QString("<< endrecord >>").toStdString().c_str());
	av_dump_format(outCtx.ctx, 0, outCtx.ctx->filename, 1);
	av_write_trailer(outCtx.ctx);
	avio_close(outCtx.ctx->pb);
	avformat_free_context(outCtx.ctx);
	if(ordRecord) emit recorded();
	
	if (m_devInfo.permanentRecord) {
		if (!ordRecord)
			m_mutex.unlock();
	}
	else
		m_mutex.unlock();

	outCtx.validOut = false;
}

//
void TFFMpegWorker::slotFinish()
{
	if (m_record) {
		slotEndRecord();
		LOG4CPLUS_FATAL(m_log, QString("slotFinish+").toStdString().c_str());
	}

	if (m_devInfo.permanentRecord) {
		endRecord(m_permRecCtx, false);
	}
	m_finish = true;
}

//
void TFFMpegWorker::slotBeginRecord(QString filepath)
{
	if (m_record) {
		slotEndRecord();
		LOG4CPLUS_FATAL(m_log, QString("slotBeginRecord+").toStdString().c_str());
	}
	
	LOG4CPLUS_FATAL(m_log, QString("slotBeginRecord " + filepath).toStdString().c_str());
	m_OrdRecCtx.file = filepath;

	beginRecord(m_OrdRecCtx);
	m_record = true;
}

//
void TFFMpegWorker::slotEndRecord()
{
	LOG4CPLUS_FATAL(m_log, QString("slotEndRecord+").toStdString().c_str());
	if (m_record){
		m_record = false;
		endRecord(m_OrdRecCtx);
	}
}

//
void TFFMpegWorker::slotSnapshot(QString filepath)
{
	m_snapshot = true;
	m_snapPath = filepath;
}

//
void TFFMpegWorker::makeSnapshot(AVFrame *frame)
{
	AVCodec *pOCodec;
	AVCodecContext *pOCodecCtx = avcodec_alloc_context3(NULL);
	pOCodecCtx->bit_rate = m_inCodecCtx->bit_rate;
	pOCodecCtx->width = m_inCodecCtx->width;
	pOCodecCtx->height = m_inCodecCtx->height;
	pOCodecCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
	pOCodecCtx->codec_id = AV_CODEC_ID_MJPEG;
	pOCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	pOCodecCtx->time_base.num = m_inCodecCtx->time_base.num;
	pOCodecCtx->time_base.den = m_inCodecCtx->time_base.den;

	pOCodec = avcodec_find_encoder(pOCodecCtx->codec_id);
	int res = avcodec_open2(pOCodecCtx, pOCodec,0);

	pOCodecCtx->qmin = pOCodecCtx->qmax = 3;
	pOCodecCtx->mb_lmin = pOCodecCtx->lmin = pOCodecCtx->qmin * FF_QP2LAMBDA;
	pOCodecCtx->mb_lmax = pOCodecCtx->lmax = pOCodecCtx->qmax * FF_QP2LAMBDA;
	pOCodecCtx->flags |= CODEC_FLAG_QSCALE;
	
	frame->quality = 1;
	frame->pts = 0;

	AVPacket pkt;
	av_init_packet(&pkt);
	pkt.data = NULL;
	pkt.size = 0;
	int gotpacket;
	res = avcodec_encode_video2(pOCodecCtx, &pkt, frame, &gotpacket);

	if (res == 0) {
		FILE *fdJPEG = fopen(m_snapPath.toLatin1(), "wb");
		int bRet = fwrite(pkt.data, sizeof(uint8_t), pkt.size, fdJPEG);
		fclose(fdJPEG);
		m_snapshot = false;
	}
	else{
		LOG4CPLUS_ERROR(m_log, QString("makeSnaphot: avcodec_encode_video2  res == %1").arg(res).toStdString().c_str());
	}
	av_free_packet(&pkt);
	avcodec_close(pOCodecCtx);
}

//
TFFMpegWorker::~TFFMpegWorker()
{
}
