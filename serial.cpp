#include "serial.h"

QT_USE_NAMESPACE

/////////////////////////////////////////////////////////////////////////////////////////
// SerialThread /////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
SerialThread::SerialThread(QObject *parent)
: QThread(parent), waitTimeout(0), quit(false)
, m_bounceTimeout(500)
, m_lastBounceTime(QTime::currentTime())
, m_stateChanged(false)
, m_blinking(false)
, m_blinkState(false)
	, m_opened(false)
{
	m_blinkTimer.setInterval(300);
	m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("SerialThread"));
	connect(&m_blinkTimer, SIGNAL(timeout()), SLOT(slotBlinkTimeout()));
}

//
SerialThread::~SerialThread()
{
	mutex.lock();
	quit = true;
	mutex.unlock();
	wait();
}

//
void SerialThread::slotBlinkTimeout()
{
	if (!m_blinking) return;
	m_blinkState ? slotSetDTR(false) : slotSetDTR(true);
	m_blinkState = !m_blinkState;
}

//
void SerialThread::startSlave(const QString &portName, int waitTimeout, int bounceTime, bool blink)
{
	QMutexLocker locker(&mutex);
	this->portName = portName;
	this->waitTimeout = waitTimeout;
	this->m_blinkOn = blink;
	this->m_bounceTimeout = bounceTime;

	if (!isRunning())
		start();
}

//
void SerialThread::run()
{
	bool currentPortNameChanged = false;

	mutex.lock();
	QString currentPortName;
	if (currentPortName != portName) {
		currentPortName = portName;
		currentPortNameChanged = true;
	}

	int currentWaitTimeout = waitTimeout;
	mutex.unlock();

	m_serial = new QSerialPort;
	m_ctsState = false;

	while (!quit) {
		if (currentPortNameChanged) {
			qDebug() << "pname " << currentPortName;
			m_serial->close();
			m_serial->setPortName(currentPortName);
			m_serial->setSettingsRestoredOnClose(false);
			m_serial->setFlowControl(QSerialPort::NoFlowControl);

			if (!m_serial->open(QIODevice::ReadWrite)) {
				emit error(tr("Can't open %1, error code %2")
						   .arg(portName).arg(m_serial->error()));
				return;
			}
			m_serial->setRequestToSend(true);
			slotSetDTR(false);
			emit connected();
			currentPortNameChanged = false;
		}

		::Sleep(100);
		
		auto pinsignals = m_serial->pinoutSignals();
		bool ctsState = pinsignals & QSerialPort::ClearToSendSignal;

		if (ctsState != m_ctsState){
			m_lastBounceTime.start();
			m_ctsState = ctsState;
			m_stateChanged = true;
			continue;
		}

		if (m_stateChanged && (m_lastBounceTime.elapsed() > m_bounceTimeout)){
			emit ctsChanged(m_ctsState);
			if (!m_ctsState)
				qDebug() << "CTS_CHANGED";
			m_stateChanged = false;
		}


		/**
		if (pinsignals & QSerialPort::ClearToSendSignal) {
			emit timeout(tr("Wait write response timeout %1")
							 .arg(QTime::currentTime().toString()));
			}
		} else {
			emit timeout(tr("Wait read request timeout %1")
						 .arg(QTime::currentTime().toString()));
		}/**/

		mutex.lock();
		if (currentPortName != portName) {
			currentPortName = portName;
			currentPortNameChanged = true;
		} else {
			currentPortNameChanged = false;
		}
		currentWaitTimeout = waitTimeout;
		mutex.unlock();
	}

	m_serial->close();
	delete m_serial;
}

//
void SerialThread::slotBlinkFilmBegin()
{
	slotSetDTR(true);
	qDebug() << "slotBlinkFilmBegin";
	if (m_blinkOn) {
		m_blinking = true;
		m_blinkState = 1;
		m_blinkTimer.start();
	}
}

//
void SerialThread::slotBlinkFilmEnd()
{
	slotSetDTR(false);
	qDebug() << "slotBlinkFilmEnd";
	if (m_blinkOn) {
		m_blinking = false;
		m_blinkTimer.stop();
	}
}

//
void SerialThread::slotBlinkImage()
{
	bool needDTR = m_serial->isDataTerminalReady();
	int ms = 0;
	for (int i = 0; i < 3; ++i) {
		QTimer::singleShot(ms,			[this] {slotSetDTR(true); } );
		QTimer::singleShot(ms += 200,	[this] {slotSetDTR(false); });
		ms += 100;
	}
	if( needDTR ) QTimer::singleShot(ms, [this] {slotSetDTR(true); });
}

//
void SerialThread::slotTimeout()
{

}

//
void SerialThread::slotSetDTR(bool state)
{
	::EscapeCommFunction(m_serial->handle(), state ? SETDTR : CLRDTR );
}