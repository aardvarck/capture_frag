#include "dxutils.h"

#ifdef USE_DIRECTX_CAPTURE

//
CComPtr<IBaseFilter> CreateFilterByName(wchar_t *filterName, const GUID &category)
{
	HRESULT hr = S_OK;
	CComPtr<ICreateDevEnum> pSysDevEnum;
	hr = pSysDevEnum.CoCreateInstance(CLSID_SystemDeviceEnum);
	if (!SUCCEEDED(hr)) return NULL;

	CComPtr<IEnumMoniker> pEnumCat;
	hr = pSysDevEnum->CreateClassEnumerator(category, &pEnumCat, 0);

	if (hr == S_OK)
	{
		CComPtr<IMoniker> pMoniker;
		ULONG cFetched;
		while (pEnumCat->Next(1, &pMoniker, &cFetched) == S_OK)
		{
			CComPtr<IPropertyBag> pPropBag;
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void **)&pPropBag);
			if (SUCCEEDED(hr))
			{
				VARIANT varName;
				VariantInit(&varName);
				hr = pPropBag->Read(L"FriendlyName", &varName, 0);
				if (SUCCEEDED(hr))
				{
					if (wcscmp(filterName, varName.bstrVal) == 0) {
						CComPtr<IBaseFilter> pFilter;
						hr = pMoniker->BindToObject(NULL, NULL, IID_IBaseFilter, (void**)&pFilter);
						if (!SUCCEEDED(hr)) return NULL;
						return pFilter;
					}
				}
				VariantClear(&varName);
			}
			pMoniker.Release();
		}
	}
	return NULL;
}

//
HRESULT IsPinConnected(IPin *pPin, BOOL *pResult)
{
	CComPtr<IPin> pTmp;
	HRESULT hr = pPin->ConnectedTo(&pTmp);
	if (SUCCEEDED(hr))
	{
		*pResult = TRUE;
	}
	else if (hr == VFW_E_NOT_CONNECTED)
	{
		// The pin is not connected. This is not an error for our purposes.
		*pResult = FALSE;
		hr = S_OK;
	}

	return hr;
}

//
IPin *GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, bool skipConnected)
{
	BOOL bFound = FALSE;
	CComPtr<IEnumPins>	pEnum;
	CComPtr<IPin>		pPin;

	pFilter->EnumPins(&pEnum);
	while (pEnum->Next(1, &pPin, 0) == S_OK)
	{
		PIN_DIRECTION PinDirThis;
		pPin->QueryDirection(&PinDirThis);
		if (bFound = (PinDir == PinDirThis)){
			BOOL res;
			IsPinConnected(pPin, &res);
			if (!skipConnected || !res)
				break;
		}
		pPin.Release();
	}
	return (bFound ? pPin : 0);
}

//
HRESULT SetProperty(IBaseFilter *filter, const QString &name, const QVariant &value)
{
	HRESULT hr;
	CComPtr<IPropertyBag> bag;
	hr = filter->QueryInterface( IID_IPropertyBag, (void **)&bag );
	
	if (!SUCCEEDED(hr))
		return hr;

	USES_CONVERSION;
	LPCOLESTR propName = A2COLE(name.toLocal8Bit().constData());
	
	VARIANT var;
	switch (value.type())
	{
	case QVariant::Int:
		var.iVal = value.toInt();
		break;
	default:
		break;
	}

	hr = bag->Write(propName, &var);
	return hr;
}

//
HRESULT LoadFileSource(IBaseFilter *filter, const QString &path)
{
	HRESULT hr;
	CComPtr<IFileSourceFilter> sf;
	hr = filter->QueryInterface(IID_IFileSourceFilter, (void**)&sf);
	if( !SUCCEEDED(hr) )
		return hr;

	AM_MEDIA_TYPE pmt;
	USES_CONVERSION;
//	QString tmppath = path;
//	QString tmppath = "URL=" + path;
	QString tmppath = "URL=rtsp://192.168.128.140:554/sdi | Transport = TCP";// | BufferLength = 10";
	LPCOLESTR fsPath = A2COLE(tmppath.toLatin1().constData());

	LPOLESTR curFile;
	hr = sf->GetCurFile( &curFile, NULL );
	
	if( curFile == NULL || 0 != wcscmp(fsPath, curFile) )
		hr = sf->Load(fsPath, &pmt);

	CoTaskMemFree(curFile);
	return hr;
}

//
HRESULT SetFileName(IPin *outPin, IBaseFilter *filter, const QString &path)
{
	HRESULT hr;
	CComPtr<IFileSinkFilter> fsink;
	hr = filter->QueryInterface(IID_IFileSinkFilter, (void**)&fsink);

	if (!SUCCEEDED(hr))
		return hr;

	CComPtr<IEnumMediaTypes> types;
	hr = outPin->EnumMediaTypes(&types);

	if (!SUCCEEDED(hr))
		return hr;

	AM_MEDIA_TYPE *type;
	if (S_OK == types->Next(1, &type, NULL)){
		USES_CONVERSION;
		LPCOLESTR filePath = A2COLE( path.toLatin1().constData() );
		hr = fsink->SetFileName(filePath, type);
	}
	return hr;
}

//
HRESULT InitializeWindowlessVMR( HWND hwndApp, IBaseFilter *vmr )
{
	HRESULT hr;
	CComPtr<IVMRFilterConfig9> pConfig;
	hr = vmr->QueryInterface(IID_IVMRFilterConfig9, (void**)&pConfig);
	if (!SUCCEEDED(hr))
		return hr;

//	hr = pConfig->SetNumberOfStreams(1);
//	return S_OK;

	DWORD mode;
	hr = pConfig->GetRenderingMode(&mode);
	if (mode != VMRMode_Windowless)
		hr = pConfig->SetRenderingMode(VMRMode_Windowless);


	CComPtr<IVMRWindowlessControl9> pwc;
	hr = vmr->QueryInterface(IID_IVMRWindowlessControl9, (void**)&pwc);
	if (!SUCCEEDED(hr))
		return hr;
	
	hr = pwc->SetVideoClippingWindow(hwndApp);
	return hr;
}

//
void registerGraphAsPublic(CComPtr<IGraphBuilder> grf)
{
	IMoniker * pMoniker;
	IRunningObjectTable *pROT;
	WCHAR wsz[128];
	IGraphBuilder *graf = grf;
	HRESULT hr = ::GetRunningObjectTable(0, &pROT);
	hr = StringCchPrintfW(wsz, NUMELMS(wsz), L"FilterGraph %08x pid %08x\0", (DWORD_PTR)graf, GetCurrentProcessId());
	hr = CreateItemMoniker(L"!", wsz, &pMoniker);
	if (SUCCEEDED(hr)){
		DWORD ss = ROTFLAGS_REGISTRATIONKEEPSALIVE;
		hr = pROT->Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, graf, pMoniker, &ss);
		pMoniker->Release();
	}
	pROT->Release();
}

#endif //USE_DIRECTX_CAPTURE