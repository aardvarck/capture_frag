#include "stdafx.h"
#include "runnable.h"

#include <stdio.h>

#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "swscale.lib")

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif
#include <libavcodec\avcodec.h>
#include <libavformat\avformat.h>
#include <libswscale\swscale.h>
#include <libavutil\avutil.h>
}

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(55,28,1)
#define av_frame_alloc avcodec_alloc_frame
#define av_frame_free avcodec_free_frame
#endif

////////////////////////////////////////////////////////
// TWorkerRunnable /////////////////////////////////////
////////////////////////////////////////////////////////
//
TWorkerRunnable::TWorkerRunnable( const QString &url )
: m_url(url)
{
}


//
TWorkerRunnable::~TWorkerRunnable()
{
}

void SaveFrame(char *pFrame, int width, int height, int iFrame) {
	AVFrame *frame = (AVFrame*)pFrame;
	
	FILE *pFile;
	char szFilename[2048];
	int  y;

	// Open file
	sprintf_s(szFilename, 2048, "frame%d.ppm", iFrame);
	pFile = fopen(szFilename, "wb");
	if (pFile == NULL)
		return;

	// Write header
	fprintf(pFile, "P6\n%d %d\n255\n", width, height);

	// Write pixel data
	for (y = 0; y < height; y++)
		fwrite(frame->data[0] + y*frame->linesize[0], 1, width * 3, pFile);

	// Close file
	fclose(pFile);
}

//
void TWorkerRunnable::run()
{
	AVFormatContext *ifcx = NULL;
//	AVInputFormat *ifmt;
	AVCodecContext *iccx;
//	AVCodec *icodec;
	AVStream *ist;
	int i_index;
//	time_t timenow, timestart;
	int got_key_frame = 0;

	AVFormatContext *ofcx;
	AVOutputFormat *ofmt;
//	AVCodecContext *occx;
//	AVCodec *ocodec;
	AVStream *ost;
//	int o_index;

	AVPacket pkt;

	uint ix;

	const char *sFileInput;
	const char *sFileOutput;
	int bRunTime;

	//	if (argc != 1) {
	//		printf("Usage: %s url outfile runtime\n", sProg);
	//		return EXIT_FAILURE;
	//	}
	sFileInput = "rtsp://192.168.128.140:554/sdi";
	sFileOutput = "test.avi";
	bRunTime = 2;

	// Initialize library
	av_log_set_level(AV_LOG_DEBUG);
	av_register_all();
	avcodec_register_all();
	avformat_network_init();

	//
	// Input
	//
	AVDictionary *opts = 0;
	av_dict_set(&opts, "rtsp_transport", "tcp", 1);

	//open rtsp
	if (avformat_open_input(&ifcx, sFileInput, NULL, &opts) != 0) {
		qDebug() << "EXIT_FAILURE";
		return;
	}

	if (avformat_find_stream_info(ifcx, NULL) < 0) {
		avformat_close_input(&ifcx);
		qDebug() << "EXIT_FAILURE";
		return;
	}

	av_dump_format(ifcx, 0, 0, 0);

	sprintf_s(ifcx->filename, sizeof(ifcx->filename), "%s", sFileInput);
	//	snprintf(ifcx->filename, sizeof(ifcx->filename), "%s", sFileInput);

	//search video stream
	i_index = -1;
	for (ix = 0; ix < ifcx->nb_streams; ix++) {
		iccx = ifcx->streams[ix]->codec;
		if (iccx->codec_type == AVMEDIA_TYPE_VIDEO) {
			ist = ifcx->streams[ix];
			i_index = ix;
			break;
		}
	}
	if (i_index < 0) {
		avformat_close_input(&ifcx);
		qDebug() << "EXIT_FAILURE";
		return;
	}

	//
	// Output
	//

	//open output file
	ofmt = av_guess_format(NULL, sFileOutput, NULL);
	ofcx = avformat_alloc_context();
	ofcx->oformat = ofmt;
	avio_open2(&ofcx->pb, sFileOutput, AVIO_FLAG_WRITE, NULL, NULL);

	// Create output stream
	//ost = avformat_new_stream( ofcx, (AVCodec *) iccx->codec );
	ost = avformat_new_stream(ofcx, NULL);
	avcodec_copy_context(ost->codec, iccx);

	ost->sample_aspect_ratio.num = iccx->sample_aspect_ratio.num;
	ost->sample_aspect_ratio.den = iccx->sample_aspect_ratio.den;

	// Assume r_frame_rate is accurate
	ost->r_frame_rate = ist->r_frame_rate;
	ost->avg_frame_rate = ost->r_frame_rate;
	ost->time_base = av_inv_q(ost->r_frame_rate);
	ost->codec->time_base = ost->time_base;

	avformat_write_header(ofcx, NULL);

	sprintf_s(ofcx->filename, 1024, "%s", sFileOutput);
	//	snprintf(ofcx->filename, sizeof(ofcx->filename), "%s", sFileOutput);

	//start reading packets from stream and write them to file

	av_dump_format(ifcx, 0, ifcx->filename, 0);
	av_dump_format(ofcx, 0, ofcx->filename, 1);


	auto pCodec = avcodec_find_decoder(iccx->codec_id);

	auto pCodecCtx = avcodec_alloc_context3(pCodec);
	if (avcodec_copy_context(pCodecCtx, iccx) != 0){
		int g = 0; g++;
	}
	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0){
		int g = 0; g++;
	}

	auto sws_ctx = sws_getContext(iccx->width,
		iccx->height,
		iccx->pix_fmt,
		iccx->width,
		iccx->height,
		AV_PIX_FMT_RGB24,
		SWS_BILINEAR,
		NULL,
		NULL,
		NULL
		);

	/**/
	AVPacket emptyPacket;
	av_init_packet(&emptyPacket);
	emptyPacket.data = NULL;
	emptyPacket.size = 0;
	emptyPacket.stream_index = i_index;
	/**/

	auto pFrame = av_frame_alloc();
	auto pFrameRGB = av_frame_alloc();

	uint8_t *buffer = NULL;
	int numBytes;
	// Determine required buffer size and allocate buffer
	numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width,pCodecCtx->height);
	buffer = (uint8_t *)av_malloc(numBytes*sizeof(uint8_t));
	avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_RGB24,pCodecCtx->width, pCodecCtx->height);

	//	timestart = timenow = get_time();
	int i = 0;
	ix = 0;
	//av_read_play(context);//play RTSP (Shouldn't need this since it defaults to playing on connect)
	av_init_packet(&pkt);
	while (av_read_frame(ifcx, &pkt) >= 0 ){//&& i < 100){//timenow - timestart <= bRunTime) {
		if (pkt.stream_index == i_index) { //packet is video               
			/**
			// Make sure we start on a key frame
			if (timestart == timenow && !(pkt.flags & AV_PKT_FLAG_KEY)) {
			timestart = timenow = get_time();
			continue;
			}
			/**/
			got_key_frame = 1;

			pkt.stream_index = ost->id;

			pkt.pts = ix++;
			pkt.dts = pkt.pts;
			int frameFinished = 0;
			/**/
			int nres = 0;
			nres = avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &pkt);
	
//			nres = avcodec_decode_video2(iccx, pFrame, &frameFinished, &emptyPacket);
			if (frameFinished){
				nres = sws_scale(sws_ctx, (uint8_t const * const *)pFrame->data
							, pFrame->linesize, 0, pCodecCtx->height
							, pFrameRGB->data, pFrameRGB->linesize );
				
				char *buff = new char[numBytes];
				memcpy(buff, pFrameRGB->data[0], numBytes);
//				SaveFrame((char*)pFrameRGB, pCodecCtx->width, pCodecCtx->height, i);

				emit image( (char*)buff );
				int g = 0;
				g++;
			}
			/**/
			av_interleaved_write_frame(ofcx, &pkt);
		}
		av_free_packet(&pkt);
		av_init_packet(&pkt);

		i++;
		//		timenow = get_time();
	}
	//	av_read_pause(ifcx);
	av_write_trailer(ofcx);
	avio_close(ofcx->pb);
	avformat_free_context(ofcx);

	avformat_network_deinit();

	emit finished("");
}
