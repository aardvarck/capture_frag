#include "capture_graph.h"

/////////////////////////////////////////////////////////////////////////////////////////
// TDXGraph /////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class TDXGraph : public TCaptureGraph
{
	Q_OBJECT
public:
	TDXGraph(const QString &name);
	~TDXGraph();
	//
	virtual void start();
	virtual void beginRecord(const QString &path);
	virtual void endRecord();
	virtual void takeSnapshot(const QString &path);
	virtual void stop(){};

private:
	HRESULT reconnectMatroskaPin();
	CComPtr<IMediaControl> getMediaControl();
	HRESULT buildGraph();
	HRESULT disconnectAllPins();
	HRESULT updateWindow();
	HRESULT prepareFileOutput();
	//
	QString m_vp;
	bool m_windowless;
	CComPtr<IPin> m_matroskaIn, m_teeCapOut;
	CComPtr<IGraphBuilder> m_graph;
	CComPtr<IBaseFilter> m_capture, m_avidec, m_render, m_tee, m_matroska, m_file;
	CComPtr<IBaseFilter> m_x264, m_avimux;
};