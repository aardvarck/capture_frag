#pragma once

#include "ConfigCapture.h"

class TDeviceWidget : public QWidget
{
	Q_OBJECT
public:
	TDeviceWidget(const QString &name, const QString &devid, const QString &profileId, QWidget *parent = 0);
	~TDeviceWidget();
	//
	void setActive(bool active);
	void setDefault(bool def);
	QString devId() { return m_devid; }
	void setName(const QString &name) { m_name = name; m_label->setText(m_name); }

signals:
	void settings( const QString &devid);
	void remove(const QString &devid);
	void defaultDev(const QString &devid);

private:
	QString		m_name, m_devid, m_profileId;
	QLabel		*m_label, *m_img;
	QPushButton *m_settings, *m_delete, *m_def;
};


/////////////////////////////////////////////////////////////////////////////////////////
// ManagerDialog ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
typedef QPair<QListWidgetItem*, TDeviceWidget*> WidItemDevPair;
class ManagerDialog : public QDialog
{
	Q_OBJECT
public:
	ManagerDialog(const QString &profileId, const QString &devid, QWidget *parent = 0);
	~ManagerDialog();
	//
	QString selectedId;

signals:
	void activateDevice( const QString &devid);

public slots:
	void updateDevices();

private slots:
	void dblclicked( QListWidgetItem *item );
	void addDevice();
	void slotSettings(QString devid);
	void slotRemove(QString devid);
	void slotDefaultDev(QString devid);

private:
	void fillFields();
	void addDeviceItem(const QString &name, const QString &id);
	//
	QMap<QString, WidItemDevPair> m_items;
	QString m_devid, m_profileId;
	QListWidget *m_deviceList;
	QPushButton *m_addNewDevice;
};