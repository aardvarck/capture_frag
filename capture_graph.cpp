#include "capture_graph.h"
#include "ffmpeg-grf.h"

#ifdef USE_DIRECTX_CAPTURE
	#include "dxgraph.h"
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// TGraphFactory ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
TGraphFactory& TGraphFactory::instance()
{
	static TGraphFactory factory;
	return factory;
}

//
TGraphFactory::TGraphFactory()
{
	m_list.push_back(std::shared_ptr<TCaptureGraph>(new TFFMpegGrf("rtsp")));
	
#ifdef USE_DIRECTX_CAPTURE
	m_list.push_back(std::shared_ptr<TCaptureGraph>(new TDXGraph("directx")));
#endif 
}

//
TGraphFactory::~TGraphFactory()
{
}

//
QStringList TGraphFactory::graphList()
{
	QStringList list;
	foreach(auto grf, m_list)
		list << grf->name();
	return list;
}

//
std::shared_ptr<TCaptureGraph> TGraphFactory::graph(const QString &name)
{
	foreach(auto grf, m_list)
		if (grf->name().compare(name,Qt::CaseInsensitive) == 0)
			return grf;
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////
// TCaptureGraph ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
TCaptureGraph::TCaptureGraph(const QString &name)
	: m_hwnd(NULL)
	, m_previewWidth(-1), m_previewHeight(-1)
	, m_name(name)
{
	m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("CaptureGraph"));
	connect( &m_restartTimer, SIGNAL(timeout()), SLOT(slotRestart()) );
}

//
TCaptureGraph::~TCaptureGraph()
{}

//
void TCaptureGraph::slotRestart()
{
	LOG4CPLUS_DEBUG(m_log, QString("TCaptureGraph::slotRestart.").toStdString().c_str());
	m_restartTimer.stop();
	endRecord();
	++m_restartCount;
	beginRecord( m_vpath + QString("_%1").arg(m_restartCount));
	m_restartTimer.start();
}

//
void TCaptureGraph::beginVideoRecord(const QString &path)
{
	m_vpath = path;
	beginRecord(m_vpath);
	m_restartTimer.start();
	m_restartCount = 0;
}

//
void TCaptureGraph::endVideoRecord()
{
	m_restartTimer.stop();
	endRecord();
}