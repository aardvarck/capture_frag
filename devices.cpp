#include "devices.h"

//
DeviceInfo makeDefaultDevice(const QString &id)
{
	DeviceInfo devinfo;
	devinfo.id = id;
	devinfo.name = QObject::tr("Vidar Capture");
	devinfo.sourceMode = "rtsp";

	devinfo.urls["rtsp"] = "rtsp://192.168.128.168/sdi";

	devinfo.serialPort = "com1";

	devinfo.frameInterval = 2;
	devinfo.postProcessing = false;
	devinfo.ffmpegArgs = "";

	devinfo.doStartGet = false;
	devinfo.getUrl = "";

	devinfo.stdSource = true;
	devinfo.ip = "192.168.128.140";
	devinfo.stdSourceId = Encoders::encodersList().first();

	devinfo.permanentRecord = false;
	devinfo.permanentRecordPath = "";

	return devinfo;
}