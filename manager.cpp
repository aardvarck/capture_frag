﻿#include "settings.h"
#include "manager.h"
#include "ProgramConfig.h"
#include "devices.h"

/////////////////////////////////////////////////////////////////////////////////////////
// TDeviceWidget ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
TDeviceWidget::TDeviceWidget( const QString &name, const QString &devid, const QString &profileId, QWidget *parent)
: QWidget(parent)
, m_name(name)
, m_devid(devid)
, m_profileId(profileId)
{
	setObjectName("filmWidget");

	m_def = new QPushButton;
	m_def->setToolTip(tr("Default device"));
	m_def->setFlat(true);
	connect(m_def, &QPushButton::clicked, this, [this]() {
		emit defaultDev(m_devid);
	});

	m_img = new QLabel;
	m_img->setPixmap(QIcon(":/resources/empty").pixmap(36, 24));

	m_label = new QLabel(name);

	m_settings = new QPushButton(QIcon(":/resources/wrench"),"");
	m_settings->setToolTip(tr("Settings"));
	connect(m_settings, &QPushButton::clicked, this, [this]() {
		emit settings(m_devid);
	});

	m_delete = new QPushButton(QIcon(":/resources/recycle"),"");
	m_delete->setToolTip(tr("Delete"));
	connect(m_delete, &QPushButton::clicked, this, [this]() {
		emit remove(m_devid);
	});

	QHBoxLayout *hl = new QHBoxLayout;
	hl->addWidget(m_def);
	hl->addWidget(m_img);
	hl->addWidget(m_label);
	hl->addStretch();
	hl->addWidget(m_settings);
	hl->addWidget(m_delete);
	
	setLayout(hl);
}

//
TDeviceWidget::~TDeviceWidget()
{
}

//
void TDeviceWidget::setActive(bool active)
{
	m_img->setPixmap(QIcon(active ? ":/resources/filled" : ":/resources/empty").pixmap(36, 24));
}

//
void TDeviceWidget::setDefault(bool def)
{
	m_def->setIcon(QIcon(def ? ":/resources/starsel" : ":/resources/starem").pixmap(36, 24));
}

/////////////////////////////////////////////////////////////////////////////////////////
// ManagerDialog ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
ManagerDialog::ManagerDialog(const QString &profileId, const QString &devid, QWidget *parent /* = 0 */)
: QDialog(parent)
, m_devid(devid)
, m_profileId(profileId)
{
	setWindowTitle(tr("Playlist"));
//	setWindowIcon(QIcon(":/resources/settings.png"));
	setModal(true);
	
	m_deviceList = new QListWidget;

	m_addNewDevice = new QPushButton(tr("Add New Device"));
	connect( m_addNewDevice, SIGNAL(clicked()), SLOT(addDevice()) );

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addWidget(m_deviceList);
	vl->addWidget(m_addNewDevice);

	setLayout(vl);

	connect(m_deviceList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(dblclicked(QListWidgetItem*)));

	fillFields();

	auto cfg = ProgramConfig::instance().capture();
	m_devid = cfg->startId;
	if( m_items.contains(m_devid) )
		m_items[m_devid].second->setActive(true);

	resize(500, 400);
	setModal(true);
}

//
ManagerDialog::~ManagerDialog()
{

}

//
void ManagerDialog::updateDevices()
{
	auto cfg = ProgramConfig::instance().capture();
	foreach(const QString &str, m_items.keys()) {
		auto pair = m_items[str];
		pair.second->setName(cfg->deviceInfo[str].name);
	}
}

//
void ManagerDialog::addDevice()
{
	QString text = QInputDialog::getText(this, tr("New device"), tr("Enter device name"));
	if (text.isEmpty()) return;

	auto cfg = ProgramConfig::instance().capture();
	
	QString newId = QDateTime::currentDateTime().toString("zzzssmmddMMyyyy");
	DeviceInfo newDevice;
	if (cfg->deviceInfo.contains(m_devid)) {
		const DeviceInfo &devinfo = cfg->deviceInfo[m_devid];
		newDevice = devinfo;
		newDevice.id = newId;
	}
	else {
		newDevice = makeDefaultDevice(newId);
		m_devid = newId;
		cfg->startId = newId;
		QTimer::singleShot( 300, this, [=]() {
			slotSettings(newId);
		});
	}
	
	newDevice.name = text;
	cfg->deviceInfo[newId] = newDevice;

	addDeviceItem(newDevice.name, newDevice.id);

	ProgramConfig::instance().saveConfig();
}

//
void ManagerDialog::dblclicked(QListWidgetItem* item)
{
	auto id = dynamic_cast<TDeviceWidget*>(m_deviceList->itemWidget(item))->devId();
	
	m_items[m_devid].second->setActive(false);
	m_devid = id;
	m_items[m_devid].second->setActive(true);

	emit activateDevice(id);
}

//
void ManagerDialog::fillFields()
{
	auto cfg = ProgramConfig::instance().capture();

	foreach(const DeviceInfo &device, cfg->deviceInfo.values())
		addDeviceItem( device.name, device.id );
}

//
void ManagerDialog::addDeviceItem( const QString &name, const QString &id )
{
	TDeviceWidget *dw = new TDeviceWidget(name, id, m_profileId);
	QListWidgetItem *item = new QListWidgetItem();
	item->setSizeHint(dw->sizeHint());
	m_deviceList->addItem(item);
	m_deviceList->setItemWidget(item, dw);

	m_items[id] = WidItemDevPair(item, dw);

	connect(dw, SIGNAL(settings(QString)), SLOT(slotSettings(QString)));
	connect(dw, SIGNAL(remove(QString)), SLOT(slotRemove(QString)));
	connect(dw, SIGNAL(defaultDev(QString)), SLOT(slotDefaultDev(QString)));

	auto cfg = ProgramConfig::instance().capture();
	dw->setDefault( cfg->startId == id );
}

//
void ManagerDialog::slotSettings(QString devid)
{
	auto cfg = ProgramConfig::instance().capture();

	SettingsDialog dialog(m_profileId, devid, this);
	dialog.restoreGeometry(ProgramConfig::instance().capture()->settingsGeometry);
	dialog.exec();
	updateDevices();

	if (cfg->deviceInfo.count() == 1)
		dblclicked(m_items[devid].first);
}

//
void ManagerDialog::slotRemove( QString devid)
{
	auto cfg = ProgramConfig::instance().capture();

	auto res = QMessageBox::question(this, cfg->deviceInfo[devid].name
		, tr("Remove this device?"), QMessageBox::Yes, QMessageBox::No, QMessageBox::NoButton);
	if (res == QMessageBox::No) return;

	cfg->deviceInfo.remove(devid);
	if (cfg->startId == devid) {
		if (cfg->deviceInfo.count() == 0)
			cfg->startId = "";
		else {
			m_devid = cfg->deviceInfo.firstKey();
			cfg->startId = m_devid;
			m_items[m_devid].second->setActive(true);
			emit activateDevice(m_devid);
		}
	}

	QListWidgetItem *it = m_items[devid].first;
	it = m_deviceList->takeItem(m_deviceList->row(it));
	delete it;
	m_items.remove(devid);

	ProgramConfig::instance().saveConfig();
}

//
void ManagerDialog::slotDefaultDev(QString devid)
{
	auto cfg = ProgramConfig::instance().capture();

	m_items[cfg->startId].second->setDefault(false);
	m_items[devid].second->setDefault(true);
	cfg->startId = devid;
	ProgramConfig::instance().saveConfig();
}