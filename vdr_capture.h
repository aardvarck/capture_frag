#pragma once

#include "ui_vdr_capture.h"
#include "serial.h"
#include "ConfigCapture.h"
#include "capture_graph.h"

class ManagerDialog;





/////////////////////////////////////////////////////////////////////////////////////////
// vdr_capture //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class vdr_capture : public QMainWindow
{
	Q_OBJECT

public:
	vdr_capture(QWidget *parent = 0);
	~vdr_capture();
	void setProfileId(const QString &id) { m_profileId = id; }

protected:
	virtual void closeEvent(QCloseEvent* event);
	virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result);
	virtual void keyPressEvent(QKeyEvent *event);

private slots:
	void slotStartRecord();
	void slotStopRecord();
	void slotSnapshot();
	void slotClose();
	void slotShow();
	void slotModeChanged(QAction *action);
	void slotSettings();
	void slotTrayActivated(QSystemTrayIcon::ActivationReason reason);
	void slotMakeGetQuery();
	void slotLimitCheckTimeout();

private slots:
	void slotSerialCtsChanged(bool state);
	void slotSerialError(QString msg);
	void slotSerialTimeout(QString msg);
	void slotSerialConnected();

private slots:
	void slotGraphStarted();
	void slotGraphStopped();
	void slotGraphRecordBegin();
	void slotGraphRecordEnd(QString path);
	void slotGraphSnapshotTaken();

private slots:
	void slotActivateGraph();
	void slotImage(const QByteArray &img);
	void slotFindStreamFail();
	void updateGUI();
	void showInfo(const QString &text, bool toshow);
	void slotIconTimer();

private slots:
	void slotManager();
	void slotActivateDevice(const QString &devid);

private:
	void checkRecognize( QPixmap &px);
	QString constructUrl();
	//
	QString newVideoPath();
	QString newSnapshotPath();
	void loadStyles();
	void createActions();
	void loadDevice();
	void initGraph();
	void createInfoWidget();
	void connectSerialSignals();
	QWidget *initQtAVPlayer(); 
	void loadQtAVPlayer();
	//
	QWidget *m_videoWidget;
	QTimer m_activateTimer, m_reciconTimer, m_rebootTimer, m_limitCheckTimer;
	QActionGroup *m_modeGroup;
	QAction *m_start, *m_stop, *m_snap, *m_snapTray, *m_close, *m_show
		, *m_settingsAct, *m_modeVideo, *m_modeImage;
	QAction *m_manager;
	QToolButton *m_modeMenuButton;
	QMenu *m_modeMenu;
	QLabel *m_infoLabel;
	QSystemTrayIcon *m_tray;
	Ui::vdr_captureClass ui;
	log4cplus::Logger m_log;

	//QtAV::VideoOutput *m_vo;
	//QtAV::AVPlayer *m_player;
	//
	ManagerDialog *m_managerDialog;
	DeviceInfo m_deviceInfo;
	QString m_profileId, m_devid;
	QString m_tempPath, m_getUrl;
	bool m_ffmpegProcReady;
	ECaptureMode m_currentMode;
	std::shared_ptr<QNetworkAccessManager> m_nam;
	std::shared_ptr<SerialThread> m_serial;
	std::shared_ptr<TCaptureGraph> m_graph;
	bool m_prepared, m_recording, m_waitToQuit;
	bool m_startedByRecog;
	bool qtavwidget = false;
	std::shared_ptr<QtAV::AVPlayer> m_player;
};