﻿#include "settings.h"
#include "capture_graph.h"
#include "ProgramConfig.h"
#include "devices.h"

QWidget *lineSeparator()
{
	auto line = new QFrame;
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	return line;
}

/////////////////////////////////////////////////////////////////////////////////////////
// SettingsDialog ///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
SettingsDialog::SettingsDialog(const QString &profileId, const QString &devid, QWidget *parent /* = 0 */)
	: QDialog(parent)
	, m_profileId(profileId)
	, m_devid(devid)
{
	setWindowTitle(tr("Settings"));
	setWindowIcon(QIcon(":/resources/settings.png"));
	setModal(true);
	
	m_save = new QPushButton(tr("Save"));
	connect(m_save, SIGNAL(clicked()),SLOT(slotSave()));
	m_cancel = new QPushButton(tr("Cancel"));
	connect(m_cancel, SIGNAL(clicked()), SLOT(reject()));
	m_savePosition = new QPushButton(tr("Save windows position"));
	connect(m_savePosition, SIGNAL(clicked()), SLOT(slotSavePosition()));
	QHBoxLayout *button_layout = new QHBoxLayout;
	button_layout->addWidget(m_cancel);
	button_layout->addStretch();
	button_layout->addWidget(m_savePosition);
	button_layout->addStretch();
	button_layout->addWidget(m_save);

	m_stacked = new QStackedWidget;
	m_stacked->addWidget( tabDevice() );
	m_stacked->addWidget( tabExtended() );
	m_stacked->addWidget( tabCommon() );
	m_stacked->addWidget( tabRecog() );

	m_tabs = new QListWidget;
	m_tabs->addItems(QStringList() << tr("Device") << tr("Extended") << tr("Common") << tr("Experiment"));
	m_tabs->setMinimumWidth(m_tabs->sizeHintForColumn(0) + 10 + 2 * m_tabs->frameWidth());
	m_tabs->setCurrentRow(0);
	connect(m_tabs, &QListWidget::itemClicked,
		this, [this](QListWidgetItem* item) { m_stacked->setCurrentIndex(m_tabs->row(item)); });

	QHBoxLayout *hl = new QHBoxLayout;
	hl->addWidget(m_tabs);
	hl->addWidget(m_stacked);

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout(hl);
	//vl->addStretch();
	vl->addLayout(button_layout);
	setLayout(vl);

	fillFields();

	m_save->setEnabled(false);
}

//
SettingsDialog::~SettingsDialog()
{
}

//
QWidget * SettingsDialog::tabDevice()
{
	QWidget *tab = new QWidget;

	m_sources = new QComboBox;
	connect(m_sources, SIGNAL(currentIndexChanged(int)), SLOT(slotSourceChanged(int)));
	m_name = new QLineEdit;
	connect(m_name, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_sourcesLabel = new QLabel(tr("Choose capture method:"));
	QHBoxLayout *src_layout = new QHBoxLayout;
	src_layout->addWidget(new QLabel(tr("Name:")));
	src_layout->addWidget(m_name);
	if (m_profileId != "")
		src_layout->addWidget(new QLabel(tr("Id:%1").arg(m_profileId)));
	src_layout->addStretch();
	src_layout->addWidget(m_sourcesLabel);
	src_layout->addWidget(m_sources);
	
	m_url = new QLineEdit;
	connect(m_url, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	QHBoxLayout *url_layout = new QHBoxLayout;
	url_layout->addWidget(new QLabel(tr("URL:")));
	url_layout->addWidget(m_url);
	url_layout->setContentsMargins(20, 0,0,0);
	m_sourceInfo = new QLabel;
	m_sourceInfo->setStyleSheet("QLabel{border:1px solid green;font:15px;}");
	m_sourceInfo->setTextInteractionFlags(Qt::TextSelectableByMouse);
	QHBoxLayout *srcinfo_layout = new QHBoxLayout;
	srcinfo_layout->addWidget(m_sourceInfo);
	srcinfo_layout->setContentsMargins(20, 0, 0, 0);

	m_stdSources = new QRadioButton(tr("Standart sources"));
	m_customSources = new QRadioButton(tr("Custom sources"));
	connect(m_stdSources, &QRadioButton::toggled, this, 
		[this](bool state) {
		if (state) {
			m_url->setDisabled(true);m_sourceInfo->setDisabled(true);
			m_srcIp->setEnabled(true); m_srcStds->setEnabled(true);
			m_customSources->setChecked(false);
			slotChanged();
		}
	});
	connect(m_customSources, &QRadioButton::toggled, this,
		[this](bool state) {
		if (state) {
			m_url->setEnabled(true); m_sourceInfo->setEnabled(true);
			m_srcIp->setEnabled(false); m_srcStds->setEnabled(false);
			m_stdSources->setChecked(false);
			slotChanged();
		}
	});

	m_srcIp = new QLineEdit;
	connect(m_srcIp, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_srcStds = new QComboBox;
	m_srcStds->addItems( Encoders::encodersList() );
	connect(m_srcStds, SIGNAL(currentIndexChanged(int)), SLOT(slotChanged()));

	QHBoxLayout *stdsrc_layout = new QHBoxLayout;
	stdsrc_layout->addWidget( new QLabel(tr("IP:")) );
	stdsrc_layout->addWidget(m_srcIp);
	stdsrc_layout->addStretch();
	stdsrc_layout->addWidget(m_srcStds);
	stdsrc_layout->addStretch();
	stdsrc_layout->setContentsMargins(20, 0, 0, 0);

	m_port = new QComboBox;
	slotUpdatePorts();
	connect(m_port, SIGNAL(currentIndexChanged(const QString&)), SLOT(slotChanged()));
	m_updatePorts = new QPushButton(tr("Update"));
	connect(m_updatePorts, SIGNAL(clicked()), SLOT(slotUpdatePorts()));
	QHBoxLayout *port_hold_layout = new QHBoxLayout;
	port_hold_layout->addWidget(new QLabel(tr("Port:")));
	port_hold_layout->addWidget(m_port);
	port_hold_layout->addWidget(m_updatePorts);
	port_hold_layout->addStretch();

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout(src_layout);
	vl->addWidget(lineSeparator());
	
	QVBoxLayout *vbb = new QVBoxLayout;
	vbb->addWidget(m_stdSources);
	vbb->addLayout(stdsrc_layout);
	vbb->addWidget(m_customSources);
	vbb->addLayout(url_layout);
	vbb->addLayout(srcinfo_layout);
	QGroupBox *sourcesBox = new QGroupBox;
	sourcesBox->setLayout(vbb);
	vl->addWidget(sourcesBox);

	vl->addWidget(lineSeparator());
	vl->addLayout(port_hold_layout);
	vl->addStretch();
	
	tab->setLayout(vl);
	return tab;
}

//
void SettingsDialog::browse(const QString &txt, QLineEdit *edit)
{
	auto nw = QFileDialog::getExistingDirectory(0, txt, edit->text());
	if (!nw.isEmpty())
		edit->setText(nw);
};

//
void SettingsDialog::slotUpdatePorts()
{
	QStringList ports;
	for (auto i: QSerialPortInfo::availablePorts())
		ports << i.portName();

/*	TCHAR portname[1024];
	for (int i = 0; i < 20; ++i) {
		_stprintf_l(portname, L"\\.\\COM%d", NULL, i);
		HANDLE port = ::CreateFile(portname, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (port == INVALID_HANDLE_VALUE) {
			DWORD err = ::GetLastError();
			if (err != ERROR_ACCESS_DENIED)
				continue;
		}
		::CloseHandle(port);
		ports << QString("COM%1").arg(i);
	}*/

	m_port->clear();
	m_port->insertItems(0, ports);
}



//
QWidget * SettingsDialog::tabExtended()
{
	QWidget *tab = new QWidget;

	m_useAuth = new QCheckBox(tr("Auth"));
	connect(m_useAuth, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_useAuth, &QCheckBox::stateChanged, this, [&](int state) {
		m_loginLabel->setEnabled(state);
		m_login->setEnabled(state);
		m_passwordLabel->setEnabled(state);
		m_password->setEnabled(state);
	});
	m_loginLabel = new QLabel(tr("Login:"));
	m_login = new QLineEdit;
	connect(m_login, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_passwordLabel = new QLabel(tr("Pass:"));
	m_password = new QLineEdit;
	m_password->setEchoMode(QLineEdit::Password);
	connect(m_password, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	QHBoxLayout *auth_layout = new QHBoxLayout;
	auth_layout->addWidget(m_useAuth);
	auth_layout->addStretch();
	auth_layout->addWidget(m_loginLabel);
	auth_layout->addWidget(m_login);
	auth_layout->addStretch();
	auth_layout->addWidget(m_passwordLabel);
	auth_layout->addWidget(m_password);
	auth_layout->addStretch();

	m_doStartGet = new QCheckBox(tr("GET url:"));
	connect(m_doStartGet, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_doStartGet, &QCheckBox::stateChanged, this, [&](int state) {
		m_getqueryUrl->setEnabled(state);
		m_getInfo->setEnabled(state);
	});
	m_getqueryUrl = new QLineEdit;
	connect(m_getqueryUrl, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	QHBoxLayout *geturl_layout = new QHBoxLayout;
	geturl_layout->addWidget(m_doStartGet);
	geturl_layout->addWidget(m_getqueryUrl);
	m_getInfo = new QLabel;
	m_getInfo->setText("For Oupree NH100S: http://admin:admin@x.x.x.x/reboot");
	m_getInfo->setStyleSheet("QLabel{border:1px solid green;font:15px;}");
	m_getInfo->setTextInteractionFlags(Qt::TextSelectableByMouse);

	m_postprocess = new QCheckBox(tr("FFmpeg post-processing"));
	connect(m_postprocess, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_postprocess, &QCheckBox::stateChanged, this, [&](int state) {
		m_ffmpegArgsLabel->setEnabled(state);
		m_ffmpegCommand->setEnabled(state);
	});

	m_ffmpegCommand = new QLineEdit;
	connect(m_ffmpegCommand, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_ffmpegArgsLabel = new QLabel(tr("Arguments:"));
	QHBoxLayout *pproc_layout = new QHBoxLayout;
	pproc_layout->addWidget(m_ffmpegArgsLabel);
	pproc_layout->addWidget(m_ffmpegCommand);

	m_frameInterval = new QSpinBox;
	m_frameInterval->setRange(0, 5);
	connect(m_frameInterval, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	m_restartMinutes = new QSpinBox;
	m_restartMinutes->setRange(1, 60);
	connect(m_restartMinutes, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *frameint_layout = new QHBoxLayout;
	frameint_layout->addWidget(new QLabel(tr("Preview update interval (sec)")));
	frameint_layout->addWidget(m_frameInterval);
	frameint_layout->addStretch();
	frameint_layout->addWidget(new QLabel(tr("Restart record (min)")));
	frameint_layout->addWidget(m_restartMinutes);
	frameint_layout->addStretch();

	m_permLabel = new QLabel(tr("Path:"));
	m_permRecPath = new QLineEdit;
	connect(m_permRecPath, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_permRecord = new QCheckBox(tr("Permanent record"));
	connect(m_permRecord, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_permRecord, &QCheckBox::stateChanged, this, [&](int state) {
		m_permLabel->setEnabled(state);
		m_permRecPath->setEnabled(state);
		m_browsePerm->setEnabled(state);
		m_permIntLabel->setEnabled(state);
		m_permRestartMinutes->setEnabled(state);
	});
	
	m_browsePerm = new QPushButton(tr("Browse"));
	connect(m_browsePerm, &QPushButton::clicked, this, [&]() { browse(tr("Choose path for video"), m_permRecPath); });
	QVBoxLayout *perm_layout = new QVBoxLayout;
	perm_layout->addWidget(m_permRecord);
	QHBoxLayout *permpath_layout = new QHBoxLayout;
	permpath_layout->addWidget(m_permLabel);
	permpath_layout->addWidget(m_permRecPath);
	permpath_layout->addWidget(m_browsePerm);
	perm_layout->addLayout(permpath_layout);

	m_permIntLabel = new QLabel(tr("Fragment duration:"));
	m_permRestartMinutes = new QSpinBox;
	m_permRestartMinutes->setRange(1, 120);
	connect(m_permRestartMinutes, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *permint_layout = new QHBoxLayout;
	permint_layout->addWidget(m_permIntLabel);
	permint_layout->addWidget(m_permRestartMinutes);
	permint_layout->addStretch();
	perm_layout->addLayout(permint_layout);

	m_fpsValue = new QSpinBox;
	m_fpsValue->setRange(1, 50);
	connect(m_fpsValue, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	m_correctFPS = new QCheckBox(tr("Correct fps"));
	connect(m_correctFPS, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_correctFPS, &QCheckBox::stateChanged, this, [&](int state) {
		m_fpsValue->setEnabled(state);
	}); 
	QHBoxLayout *correctfps_layout = new QHBoxLayout;
	correctfps_layout->addWidget(m_correctFPS);
	correctfps_layout->addWidget(m_fpsValue);
	correctfps_layout->addStretch();

	m_blinkOn = new QCheckBox(tr("Blinking light"));
	connect(m_blinkOn, SIGNAL(stateChanged(int)), SLOT(slotChanged()));

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout(auth_layout);
	vl->addWidget(lineSeparator());
	vl->addLayout(geturl_layout);
	vl->addWidget(m_getInfo);
	vl->addWidget(lineSeparator());
	vl->addWidget(m_postprocess);
	vl->addLayout(pproc_layout);
	vl->addWidget(lineSeparator());
	vl->addLayout(frameint_layout);
	vl->addWidget(lineSeparator());
	vl->addLayout(perm_layout);
	vl->addWidget(lineSeparator());
	vl->addLayout(correctfps_layout);
	vl->addWidget(lineSeparator());
	vl->addWidget(m_blinkOn);
	vl->addStretch();

	tab->setLayout(vl);
	return tab;
}

//
QWidget * SettingsDialog::tabCommon()
{
	QWidget *tab = new QWidget;
	
	m_videoMode = new QComboBox;
	connect(m_videoMode, SIGNAL(currentIndexChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *capture_mode_layout = new QHBoxLayout;
	capture_mode_layout->addWidget(new QLabel(tr("Capture mode:")));
	capture_mode_layout->addWidget(m_videoMode);

	m_holdMode = new QComboBox;
	connect(m_holdMode, SIGNAL(currentIndexChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *port_hold_layout = new QHBoxLayout;
	port_hold_layout->addWidget(new QLabel(tr("Control mode:")));
	port_hold_layout->addWidget(m_holdMode);

	m_previewMode = new QComboBox;
	connect(m_previewMode, SIGNAL(currentIndexChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *video_preview_layout = new QHBoxLayout;
	video_preview_layout->addWidget(new QLabel(tr("Preview mode:")));
	video_preview_layout->addWidget(m_previewMode);

	m_usefileprefix = new QCheckBox(tr("Use file prefix"));
	connect(m_usefileprefix, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_usefileprefix, &QCheckBox::stateChanged, this, [&](int state) {
		m_fileprefix->setEnabled(state);
	});
	m_fileprefix = new QLineEdit;
	connect(m_fileprefix, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	QHBoxLayout *prefix_layout = new QHBoxLayout;
	prefix_layout->addWidget(m_usefileprefix);
	prefix_layout->addWidget(m_fileprefix);
	
	m_vp = new QLineEdit;
	connect(m_vp, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_browseVideo = new QPushButton(tr("Browse"));
	connect(m_browseVideo, &QPushButton::clicked, this, [&]() { browse(tr("Choose path for video"), m_vp); });
	QHBoxLayout *vp_layout = new QHBoxLayout;
	vp_layout->addWidget(new QLabel(tr("Video path:")));
	vp_layout->addWidget(m_vp);
	vp_layout->addWidget(m_browseVideo);

	m_sp = new QLineEdit;
	connect(m_sp, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_browseImage = new QPushButton(tr("Browse"));
	connect(m_browseImage, &QPushButton::clicked, this, [&]() { browse(tr("Choose path for images"), m_sp); });
	QHBoxLayout *sp_layout = new QHBoxLayout;
	sp_layout->addWidget(new QLabel(tr("Image path:")));
	sp_layout->addWidget(m_sp);
	sp_layout->addWidget(m_browseImage);

	m_vpext = new QLineEdit;
	connect(m_vpext, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	m_spext = new QLineEdit;
	connect(m_spext, SIGNAL(textChanged(const QString&)), SLOT(slotChanged()));
	QHBoxLayout *ext_layout = new QHBoxLayout;
	ext_layout->addWidget(new QLabel(tr("Extensions")));
	ext_layout->addStretch();
	ext_layout->addWidget(new QLabel(tr("video:")));
	ext_layout->addWidget(m_vpext);
	ext_layout->addWidget(new QLabel(tr("image:")));
	ext_layout->addWidget(m_spext);
	ext_layout->addStretch();

	m_limitSpace = new QCheckBox(tr("Use space limit"));
	connect(m_limitSpace, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	connect(m_limitSpace, &QCheckBox::stateChanged, this, [&](int state) {
		m_limitSpaceSpin->setEnabled(state);
	});
	m_limitSpaceSpin = new QSpinBox;
	m_limitSpaceSpin->setRange(1, 1000);
	connect(m_limitSpaceSpin, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *limit_layout = new QHBoxLayout;
	limit_layout->addWidget(new QLabel(tr("Max film count:")));
	limit_layout->addWidget(m_limitSpaceSpin);
	limit_layout->addWidget(new QLabel(tr("films.")));
	limit_layout->addStretch();

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout(capture_mode_layout);
	vl->addLayout(port_hold_layout);
	vl->addLayout(video_preview_layout);
	vl->addWidget(lineSeparator());
	vl->addLayout(prefix_layout);
	vl->addLayout(vp_layout);
	vl->addLayout(sp_layout);
	vl->addLayout(ext_layout);
	vl->addWidget(lineSeparator());
	vl->addWidget(m_limitSpace);
	vl->addLayout(limit_layout);
	vl->addStretch();

	tab->setLayout(vl);
	return tab;
}

//
QWidget * SettingsDialog::tabRecog()
{
	QWidget *tab = new QWidget;

	m_black = new QSpinBox;
	m_black->setRange(0, 255);
	connect(m_black, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *black_layout = new QHBoxLayout;
	black_layout->addWidget(new QLabel(tr("Black maximum:")));
	black_layout->addWidget(m_black);
	black_layout->addStretch();

	m_red = new QSpinBox;
	m_red->setRange(0, 255);
	connect(m_red, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *red_layout = new QHBoxLayout;
	red_layout->addWidget(new QLabel(tr("Red minimum:")));
	red_layout->addWidget(m_red);
	red_layout->addStretch();

	m_redDiff = new QSpinBox;
	m_redDiff->setRange(0, 255);
	connect(m_redDiff, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *reddiff_layout = new QHBoxLayout;
	reddiff_layout->addWidget(new QLabel(tr("Red difference:")));
	reddiff_layout->addWidget(m_redDiff);
	reddiff_layout->addStretch();

	m_recogBound = new QSpinBox;
	m_recogBound->setRange(0, 100);
	connect(m_recogBound, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *recog_layout = new QHBoxLayout;
	recog_layout->addWidget(new QLabel(tr("Min red percentage:")));
	recog_layout->addWidget(m_recogBound);
	recog_layout->addStretch();

	m_recogEndBound = new QSpinBox;
	m_recogEndBound->setRange(0, 100);
	connect(m_recogEndBound, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *recog_end_layout = new QHBoxLayout;
	recog_end_layout->addWidget(new QLabel(tr("Max red end percentage:")));
	recog_end_layout->addWidget(m_recogEndBound);
	recog_end_layout->addStretch();

	m_recogTime = new QSpinBox;
	m_recogTime->setRange(0, 100);
	connect(m_recogTime, SIGNAL(valueChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *recog_time_layout = new QHBoxLayout;
	recog_time_layout->addWidget(new QLabel(tr("Recognition time:")));
	recog_time_layout->addWidget(m_recogTime);
	recog_time_layout->addStretch();

	m_recogShow = new QCheckBox;
	connect(m_recogShow, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *recog_show_layout = new QHBoxLayout;
	recog_show_layout->addWidget(new QLabel(tr("Show recog:")));
	recog_show_layout->addWidget(m_recogShow);
	recog_show_layout->addStretch();

	QVBoxLayout *vl = new QVBoxLayout;
	vl->addLayout(black_layout);
	vl->addLayout(red_layout);
	vl->addLayout(reddiff_layout);
	vl->addLayout(recog_layout);
	vl->addLayout(recog_end_layout);
	vl->addLayout(recog_time_layout);

	m_recogEnable = new QGroupBox(tr("Start recognition"));
	m_recogEnable->setCheckable(true);
	m_recogEnable->setLayout(vl);
	connect(m_recogEnable, SIGNAL(toggled(bool)), SLOT(slotChanged()));
	
	m_recogDump = new QCheckBox;
	connect(m_recogDump, SIGNAL(stateChanged(int)), SLOT(slotChanged()));
	QHBoxLayout *recog_dump_layout = new QHBoxLayout;
	recog_dump_layout->addWidget(new QLabel(tr("Dump recog:")));
	recog_dump_layout->addWidget(m_recogDump);
	recog_dump_layout->addStretch();

	QPushButton *manager_button = new QPushButton(tr("Device list"));
	connect(manager_button, SIGNAL(clicked()), SIGNAL(showDeviceList()));

	QHBoxLayout *mvl = new QHBoxLayout;
	mvl->addWidget(manager_button);
	mvl->addStretch();
	
	m_qtav = new QCheckBox;
	connect(m_qtav, &QCheckBox::stateChanged, [this](bool state) {
		m_qtavtype->setEnabled(state);
		m_qtavtypelabel->setEnabled(state);
		slotChanged();
	});

	m_qtavtypelabel = new QLabel(tr("Renderer:"));
	m_qtavtype = new QComboBox;
	connect(m_qtavtype, SIGNAL(currentIndexChanged(int)), SLOT(slotChanged()));
	
	QHBoxLayout *qtav_layout = new QHBoxLayout;
	qtav_layout->addWidget(m_qtav);
	qtav_layout->addWidget( new QLabel(tr("Use QtAv")) );
	qtav_layout->addWidget(m_qtavtypelabel);
	qtav_layout->addWidget(m_qtavtype);
	qtav_layout->addStretch();

	QVBoxLayout *vll = new QVBoxLayout;
	vll->addWidget(m_recogEnable);
	vll->addLayout(recog_show_layout);
	vll->addLayout(recog_dump_layout);
	vll->addWidget(lineSeparator());
	vll->addLayout(mvl);
	vll->addWidget(lineSeparator());
	vll->addLayout(qtav_layout);
	vll->addStretch();

	tab->setLayout(vll);

	return tab;
}

//
void SettingsDialog::slotSourceChanged(int idx)
{
	auto cfg = ProgramConfig::instance().capture();
	DeviceInfo devinfo = cfg->deviceInfo[m_devid];
	
	QString src = m_sources->itemText(idx);
	m_url->setText(devinfo.urls[src] );
	m_sourceInfo->setText(TGraphFactory::instance().graph(src)->hint());
}

//
void SettingsDialog::slotSavePosition()
{
	auto cfg = ProgramConfig::instance().capture();
	cfg->mainGeometry = parentWidget()->saveGeometry();
	cfg->settingsGeometry = saveGeometry();
	ProgramConfig::instance().saveConfig();
}

//
void SettingsDialog::slotChanged()
{
	m_valid = true;
	checkFolderExistance(m_vp);
	checkFolderExistance(m_sp);

	m_save->setEnabled(m_valid);
}

//
void SettingsDialog::checkFolderExistance( QLineEdit *edit )
{
	QDir dir(edit->text());
	if (!dir.exists()){
//		m_valid = false;
		edit->setStyleSheet("QLineEdit{background:#FF9999;}");
	}
	else
		edit->setStyleSheet("");
}

//
void SettingsDialog::slotSave()
{
	saveFields();
	accept();
}

//
void SettingsDialog::fillFields()
{
	auto cfg = ProgramConfig::instance().capture();
	const DeviceInfo &devinfo = cfg->deviceInfo[m_devid];
	
	m_name->setText(devinfo.name);
	
	m_sources->addItems(TGraphFactory::instance().graphList());
	if (m_sources->count() < 2) {
		m_sources->hide();
		m_sourcesLabel->hide();
	}

//	wait for static ffmpeg
//	m_postprocess->setChecked(devinfo.postProcessing );
	m_postprocess->setChecked(false);
	m_postprocess->setEnabled(false);
	m_ffmpegArgsLabel->setEnabled(devinfo.postProcessing);
	m_ffmpegCommand->setEnabled(devinfo.postProcessing);
	m_ffmpegCommand->setText(devinfo.ffmpegArgs );

	m_blinkOn->setChecked(devinfo.blinkOn);

	m_useAuth->setChecked(devinfo.useAuth);
	m_login->setEnabled(devinfo.useAuth); m_loginLabel->setEnabled(devinfo.useAuth);
	m_login->setText(devinfo.login);
	m_password->setEnabled(devinfo.useAuth); m_passwordLabel->setEnabled(devinfo.useAuth);
	m_password->setText(devinfo.password);
	
	m_doStartGet->setChecked(devinfo.doStartGet);
	m_getqueryUrl->setEnabled(devinfo.doStartGet);
	m_getqueryUrl->setText(devinfo.getUrl );
	m_getInfo->setEnabled(devinfo.doStartGet);

	m_frameInterval->setValue(devinfo.frameInterval);
	
	m_restartMinutes->setValue( cfg->filmRestartMinutes );
	
	m_usefileprefix->setChecked(cfg->useFilePrefix);
	m_fileprefix->setEnabled(cfg->useFilePrefix);
	m_fileprefix->setText(cfg->filePrefix);
	m_vp->setText(cfg->videoPath);
	m_sp->setText(cfg->imagePath);
	m_vpext->setText(cfg->videoExt);
	m_spext->setText(cfg->imageExt);
	m_port->setCurrentText(devinfo.serialPort);

	m_limitSpace->setChecked(cfg->limitSpace);
	m_limitSpaceSpin->setValue(cfg->limitSpaceValue);
	m_limitSpaceSpin->setEnabled(cfg->limitSpace);

	m_previewMode->addItems(QStringList() << tr("Show") << tr("Hide") << tr("ShowOnRecord"));
	m_previewMode->setCurrentIndex(cfg->previewMode);

	m_videoMode->addItems( QStringList()<<tr("Image")<<tr("Video") );
	m_videoMode->setCurrentIndex(cfg->captureMode);

	m_holdMode->addItems(QStringList() << tr("Hold for record") << tr("click-Start-click-Stop"));
	m_holdMode->setCurrentIndex(cfg->controlMode);

	m_srcIp->setText(devinfo.ip);
	m_srcStds->setCurrentText(devinfo.stdSourceId);
	m_stdSources->setChecked(devinfo.stdSource );
	m_customSources->setChecked(!devinfo.stdSource);

	m_permRecord->setChecked( devinfo.permanentRecord );
	m_permRecPath->setText( devinfo.permanentRecordPath );
	m_permLabel->setEnabled(devinfo.permanentRecord);
	m_permRecPath->setEnabled(devinfo.permanentRecord);
	m_browsePerm->setEnabled(devinfo.permanentRecord);
	m_permRestartMinutes->setEnabled(devinfo.permanentRecord);
	m_permRestartMinutes->setValue(devinfo.permRecMin);
	m_correctFPS->setChecked(devinfo.correctFrameRate);
	m_fpsValue->setValue(devinfo.frameRateValue);
	m_fpsValue->setEnabled(devinfo.correctFrameRate);

	m_black->setValue(cfg->black);
	m_red->setValue(cfg->red);
	m_redDiff->setValue(cfg->reddiff);
	m_recogBound->setValue(cfg->recogbound);
	m_recogEndBound->setValue(cfg->recogendbound);
	m_recogTime->setValue(cfg->recogtime);
	m_recogEnable->setChecked(cfg->recogenable);
	m_recogShow->setChecked(cfg->recogshow);
	m_recogDump->setChecked(cfg->recogdump);

	m_qtav->setChecked(cfg->qtavwidget);
	m_qtavtype->addItems(QStringList() << "WidgetRenderer" << "Direct2DRenderer" << "GLWidgetRenderer2");
	m_qtavtype->setCurrentIndex(cfg->qtavwidgettype);
	m_qtavtypelabel->setEnabled(cfg->qtavwidget);
	m_qtavtype->setEnabled(cfg->qtavwidget);
}

//
void SettingsDialog::saveFields()
{
	auto cfg = ProgramConfig::instance().capture();
	DeviceInfo &devinfo = cfg->deviceInfo[m_devid];
	
	QString src = m_sources->currentText().toLower();

	devinfo.urls[src] = m_url->text();

	devinfo.name = m_name->text();
	
	devinfo.sourceMode = src;

	devinfo.postProcessing = m_postprocess->isChecked();
	devinfo.ffmpegArgs = m_ffmpegCommand->text();
	devinfo.doStartGet = m_doStartGet->isChecked();
	devinfo.getUrl = m_getqueryUrl->text();
	devinfo.blinkOn = m_blinkOn->isChecked();

	devinfo.useAuth = m_useAuth->isChecked();
	devinfo.login = m_login->text();
	devinfo.password = m_password->text();

	devinfo.frameInterval = m_frameInterval->value();

	cfg->filmRestartMinutes = m_restartMinutes->value();

	QDir dir;
	cfg->useFilePrefix = m_usefileprefix->isChecked();
	cfg->filePrefix = m_fileprefix->text();
	
	cfg->videoPath = m_vp->text();
	dir.mkpath(cfg->videoPath);
	cfg->imagePath = m_sp->text();
	dir.mkpath(cfg->imagePath);

	cfg->videoExt = m_vpext->text();
	cfg->imageExt = m_spext->text();
	devinfo.serialPort = m_port->currentText();

	cfg->limitSpace = m_limitSpace->isChecked();
	cfg->limitSpaceValue = m_limitSpaceSpin->value();

	cfg->previewMode = (EPreviewMode)m_previewMode->currentIndex();
	cfg->captureMode = (ECaptureMode)m_videoMode->currentIndex();
	cfg->controlMode = (EControlMode)m_holdMode->currentIndex();

	devinfo.ip = m_srcIp->text();
	devinfo.stdSourceId = m_srcStds->currentText();
	devinfo.stdSource = m_stdSources->isChecked();

	devinfo.permanentRecord = m_permRecord->isChecked();
	devinfo.permanentRecordPath = m_permRecPath->text();
	devinfo.permRecMin = m_permRestartMinutes->value();
	devinfo.correctFrameRate = m_correctFPS->isChecked();
	devinfo.frameRateValue = m_fpsValue->value();

	cfg->black = m_black->value();
	cfg->red = m_red->value();
	cfg->reddiff = m_redDiff->value();
	cfg->recogbound = m_recogBound->value();
	cfg->recogendbound = m_recogEndBound->value();
	cfg->recogtime= m_recogTime->value();
	cfg->recogenable = m_recogEnable->isChecked();
	cfg->recogshow = m_recogShow->isChecked();
	cfg->recogdump = m_recogDump->isChecked();

	cfg->qtavwidget = m_qtav->isChecked();
	cfg->qtavwidgettype = m_qtavtype->currentIndex();

	ProgramConfig::instance().saveConfig();
}