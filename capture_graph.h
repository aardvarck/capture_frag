#pragma once

#include "ConfigCapture.h"

enum EGraphState{
	EStopped = 0
,	ERunning
,	ERecording
, ESnapshotTaken
};

class TCaptureGraph;
/////////////////////////////////////////////////////////////////////////////////////////
// TGraphFactory ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class TGraphFactory : public QObject
{
	Q_OBJECT
public:
	static TGraphFactory& instance();
	//
	TGraphFactory();
	~TGraphFactory();
	//
	QStringList graphList();
	std::shared_ptr<TCaptureGraph> graph(const QString &name);

private:
	QList<std::shared_ptr<TCaptureGraph>> m_list;
};
		
/////////////////////////////////////////////////////////////////////////////////////////
// TCaptureGraph ////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class TCaptureGraph : public QObject
{
	Q_OBJECT
public:
	TCaptureGraph(const QString &name);
	virtual ~TCaptureGraph();
	//
	QString name(){ return m_name; }
	virtual QString hint(){ return ""; }
	virtual void setUrl(const QString &url) { m_url = url; }
	virtual void setPreviewSize(int w, int h){ m_previewWidth = w; m_previewHeight = h; }
	virtual void setWindow(HWND hwnd){ m_hwnd = hwnd; }
	virtual void setRestartTimeout(int min) { m_restartTimeout = min; m_restartTimer.setInterval(min * 60 * 1000); }
//	void setExtensions(const QString &vext, const QString &sext) { m_vext = vext; m_sext = sext; }
	void setDeviceInfo(const DeviceInfo &devinfo) { m_devInfo = devinfo; }

	//
	QMutex *mutex() { return m_mutex; }

	//
	virtual void start() = 0;
	virtual void beginVideoRecord( const QString &path);
	virtual void endVideoRecord();
	virtual void takeSnapshot(const QString &path) = 0;
	virtual void stop() = 0;

protected:
	virtual void beginRecord(const QString &path) = 0;
	virtual void endRecord() = 0;

signals:
	void started();
	void stopped();
	void recordBegin();
	void recordEnd( QString path );
	void snapshotTaken();

protected slots:
	virtual void slotRestart();

protected:
	log4cplus::Logger m_log;
	HWND m_hwnd;
	QString m_name, m_url, m_vpath, m_spath;
	int m_previewWidth, m_previewHeight;
	int m_restartTimeout, m_restartCount;
	QTimer m_restartTimer;
	DeviceInfo m_devInfo;
	QMutex *m_mutex;
};