#pragma once

/////////////////////////////////////////////////////////////////////////////////////////
// SettingsDialog ///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class SettingsDialog : public QDialog
{
	Q_OBJECT

public:
	SettingsDialog(const QString &profileId, const QString &devid, QWidget *parent = 0);
	~SettingsDialog();

signals:
	void showDeviceList();
	
private slots:
	void slotChanged();
	void slotSave();
	void slotSavePosition();
	void slotSourceChanged(int idx);
	void slotUpdatePorts();

private:
	void browse(const QString &txt, QLineEdit *edit);
	void fillFields();
	void saveFields();
	void checkFolderExistance( QLineEdit *edit );
	//
	QWidget *tabDevice();
	QWidget *tabExtended();
	QWidget *tabCommon();
	QWidget *tabRecog();
	//
	QListWidget *m_tabs;
	QStackedWidget *m_stacked;
	QLineEdit *m_url, *m_fileprefix, *m_vp, *m_sp, *m_vpext, *m_spext,*m_ffmpegCommand, *m_getqueryUrl, *m_name;
	QLineEdit *m_permRecPath;
	QComboBox *m_sources, *m_videoMode, *m_holdMode, *m_previewMode;
	QLabel *m_sourcesLabel;
	QCheckBox *m_usefileprefix, *m_postprocess, *m_doStartGet, *m_permRecord;
	QPushButton *m_browseVideo, *m_browseImage, *m_browsePerm;
	QPushButton *m_cancel, *m_save, *m_savePosition;
	QLabel *m_sourceInfo, *m_ffmpegArgsLabel, *m_permLabel, *m_permIntLabel, *m_getInfo;
	QSpinBox *m_frameInterval, *m_restartMinutes, *m_permRestartMinutes;
	QLineEdit *m_srcIp;
	QComboBox *m_srcStds;
	QRadioButton *m_stdSources, *m_customSources;
	QSpinBox *m_black, *m_red, *m_redDiff, *m_colInt, *m_rowInt, *m_recogBound, *m_recogEndBound, *m_recogTime;
	QGroupBox *m_recogEnable;
	QCheckBox *m_recogShow, *m_recogDump;
	QCheckBox *m_correctFPS;
	QSpinBox *m_fpsValue;
	QCheckBox *m_useAuth;
	QLabel *m_loginLabel, *m_passwordLabel;
	QLineEdit *m_login, *m_password;
	QComboBox *m_port;
	QPushButton *m_updatePorts;
	QCheckBox *m_limitSpace;
	QSpinBox *m_limitSpaceSpin;
	QCheckBox *m_blinkOn;
	QCheckBox *m_qtav;
	QComboBox *m_qtavtype;
	QLabel *m_qtavtypelabel;
	//
	bool m_valid;
	QString m_profileId, m_devid;
};