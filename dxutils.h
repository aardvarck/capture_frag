#pragma once

CComPtr<IBaseFilter> CreateFilterByName(wchar_t *filterName, const GUID &category);

HRESULT IsPinConnected(IPin *pPin, BOOL *pResult);

IPin *GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, bool skipConnected = true);

HRESULT SetProperty(IBaseFilter *filter, const QString &name, const QVariant &value);

HRESULT LoadFileSource(IBaseFilter *filter, const QString &path);

HRESULT SetFileName(IPin *outPin, IBaseFilter *filter, const QString &path);

HRESULT InitializeWindowlessVMR(HWND hwndApp, IBaseFilter *vmr);

void registerGraphAsPublic(CComPtr<IGraphBuilder> grf);

//HRESULT MakeConnectChain( QList<> );

//HRESULT MakeDisconnectChain();

const GUID CLSID_VPP = { 0xCF5878AC, 0x78F0, 0x4544, 0x83, 0xB9, 0xA9, 0x40, 0xE2, 0x0D, 0x96, 0x90 };
const GUID CLSID_URTPSrc = { 0xF8F42929, 0xB668, 0x46D1, 0xAE, 0x0A, 0x8E, 0x9D, 0xF3, 0x3E, 0x7F, 0x5A };
const GUID CLSID_AxisRTPSrc = { 0x4f1d0c59, 0x5ecc, 0x4028, 0x87, 0xf3, 0x48, 0x21, 0x91, 0xd2, 0x23, 0x0f };
const GUID CLSID_Matroska = { 0xA28F324B, 0xDDC5, 0x4999, 0xAA, 0x25, 0xD3, 0xA7, 0xE2, 0x5E, 0xF7, 0xA8 };
const GUID CLSID_DTVDVD = { 0x212690FB, 0x83E5, 0x4526, 0x8F, 0xD7, 0x74, 0x47, 0x8B, 0x79, 0x39, 0xCD };
const GUID CLSID_GDCL = { 0x5FD85181, 0xE542, 0x4E52, 0x8D, 0x9D, 0x5D, 0x61, 0x3C, 0x30, 0x13, 0x1B };
const GUID CLSID_ffddec = { 0x04FE9017, 0xF873, 0x410E, 0x87, 0x1E, 0xAB, 0x91, 0x66, 0x1A, 0x4E, 0xF7 };

const GUID CLSID_Dazzle = { 0x17CCA71B, 0xECD7, 0x11D0, 0xB9, 0x08, 0x00, 0xA0, 0xC9, 0x22, 0x31, 0x96 };
const GUID CLSID_WDMCapDev = { 0x65E8773D, 0x8F56, 0x11D0, 0xA3, 0xB9, 0x00, 0xA0, 0xC9, 0x22, 0x31, 0x96 };
const GUID CLSID_x264 = { 0xD76E2820, 0x1563, 0x11CF, 0xAC, 0x98, 0x00, 0xAA, 0x00, 0x4C, 0x0F, 0xA9 };
const GUID CLSID_AviMux = { 0xE2510970, 0xF137, 0x11CE, 0x8B, 0x67, 0x00, 0xAA, 0x00, 0xA3, 0xF1, 0xA6 };

const GUID CLSID_MVStreamSource = { 0xEDE234EC, 0x157E, 0x4516, 0x9A, 0xC5, 0x0F, 0x40, 0x13, 0x84, 0x91, 0x8B };
const GUID CLSID_VisioForgeMP4Mux = { 0x4649106F, 0xA772, 0x4345, 0x89, 0x71, 0x61, 0x00, 0xF6, 0xEE, 0xA1, 0xF2 };