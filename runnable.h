#pragma once

////////////////////////////////////////////////////////
// TWorkerRunnable /////////////////////////////////////
////////////////////////////////////////////////////////
//
class TWorkerRunnable : public QObject, public QRunnable
{
	Q_OBJECT
public:
	TWorkerRunnable( const QString &url );
	virtual ~TWorkerRunnable();
	//
	virtual void run();

signals:
	void message( QString msg );
	void finished( QString msg );
	void image( char *frame );

private:
	QString m_url;
};