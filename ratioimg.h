////////////////////////////////////////////////////////
// TRatioImageWidget ///////////////////////////////////
////////////////////////////////////////////////////////
//
//( ty SO ^_^ http://stackoverflow.com/questions/18304355/centering-widgets-in-a-dynamic-layout)
class TRatioImageWidget : public QWidget {
public:
	//
	TRatioImageWidget(QWidget* parent = 0): QWidget(parent){}

    //
	virtual void paintEvent(QPaintEvent * e)
    {
        QRect srcRect(QPoint(), pixmap.size());
        QSize dstSize = srcRect.size().scaled(
              e->rect().size(), Qt::KeepAspectRatio);
        QRect dstRect(QPoint((width() - dstSize.width())/2,
                             (height() - dstSize.height())/2), dstSize);

        QPainter p(this);
        //p.setRenderHint(QPainter::);
        p.drawPixmap(dstRect, pixmap, srcRect);
    }
	
	//
	void setPixmap(const QPixmap &px )
	{
		pixmap = px;
		update();
	}

private:
    QPixmap pixmap;
};