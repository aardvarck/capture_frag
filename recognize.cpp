#include <stdafx.h>

#include "recognize.h"

typedef struct {
	double r;       // percent
	double g;       // percent
	double b;       // percent
} rgb;

typedef struct {
	double h;       // angle in degrees
	double s;       // percent
	double v;       // percent
} hsv;

static hsv   rgb2hsv(rgb in);
static rgb   hsv2rgb(hsv in);

hsv rgb2hsv(rgb in)
{
	hsv         out;
	double      min, max, delta;

	min = in.r < in.g ? in.r : in.g;
	min = min  < in.b ? min : in.b;

	max = in.r > in.g ? in.r : in.g;
	max = max  > in.b ? max : in.b;

	out.v = max;                                // v
	delta = max - min;
	if (delta < 0.00001)
	{
		out.s = 0;
		out.h = 0; // undefined, maybe nan?
		return out;
	}
	if (max > 0.0) { // NOTE: if Max is == 0, this divide would cause a crash
		out.s = (delta / max);                  // s
	}
	else {
		// if max is 0, then r = g = b = 0              
		// s = 0, v is undefined
		out.s = 0.0;
		out.h = NAN;                            // its now undefined
		return out;
	}
	if (in.r >= max)                           // > is bogus, just keeps compilor happy
		out.h = (in.g - in.b) / delta;        // between yellow & magenta
	else
		if (in.g >= max)
			out.h = 2.0 + (in.b - in.r) / delta;  // between cyan & yellow
		else
			out.h = 4.0 + (in.r - in.g) / delta;  // between magenta & cyan

	out.h *= 60.0;                              // degrees

	if (out.h < 0.0)
		out.h += 360.0;

	return out;
}

//
void HfromHSVisRED( QRgb color)
{
	int max = qRed(color) > qGreen(color)
		? (qRed(color) > qBlue(color) ? qRed(color) : qBlue(color))
		: (qGreen(color) > qBlue(color) ? qGreen(color) : qBlue(color));
	int min = qRed(color) < qGreen(color)
		? (qRed(color) < qBlue(color) ? qRed(color) : qBlue(color))
		: (qGreen(color) < qBlue(color) ? qGreen(color) : qBlue(color));
	int H;
	if (max == min) H = 0;
	else if (max == qRed(color)) {
		H = (float)(60 * (qGreen(color) - qBlue(color))) / (max - min);
		if (qGreen(color) < qBlue(color))
			H += 360;
	}
	else if( max == qGreen(color) )
		H = (float)(60 * (qBlue(color) - qRed(color))) / (max - min) + 120;
	else if (max == qBlue(color) )
		H = (float)(60 * (qRed(color) - qGreen(color))) / (max - min) + 240;
	else {
		H = -1;
	}

	float S;
	if (max == 0) S = 0;
	else S = 1.0f - (float)min / max;

	int V = max;


	qDebug() << QString("HSV = %6 %7 %8, max = %1, min = %2, R = %3, G = %4, B = %5")
		.arg(max).arg(min).arg(qRed(color)).arg(qGreen(color)).arg(qBlue(color)).arg(H).arg(S).arg(V);

	rgb rgbc{ (double)qRed(color), (double)qGreen(color), (double)qBlue(color) };
	hsv hsvc = rgb2hsv(rgbc);
	qDebug() << QString("HSV = %6 %7 %8 OTHER\n")
		.arg(hsvc.h).arg(hsvc.s).arg(hsvc.v);

}

/**
int hei = qim.height() >> 2;
//	const uchar *line = qim.constScanLine( qim.height() >> 2 );
QString str, cstr;
cstr = "RGB(%1,%2,%3)";
int redborder = 128;
for (int i = 0; i < qim.width(); i++) {
if (i % 10 != 0) continue;
//const uchar *cur = line +
QRgb cur = qim.pixel(i, hei);
//str += cstr.arg(qRed(cur)).arg(qGreen(cur)).arg(qBlue(cur)) + " ";
HfromHSVisRED( cur );
}
qDebug() << str << "\n\n\n";
/**/

//
bool pixIsBlack(QRgb px, int val)
{
	return qRed(px) < val && qGreen(px) < val && qBlue(px) < val;
}

//
bool pixIsRed(QRgb px, int val, int diff)
{
	return qRed(px) > val && qGreen(px) < qRed(px)-diff && qBlue(px) < qRed(px)-diff;
}

//
int detectRedRow( const QImage &img, int row, int lb, int rb, const RecogParams &params )
{
	int pxn = rb - lb, redpx = 0;
	for (int i = lb; i < rb; i++)
		if ( pixIsRed(img.pixel(i, row), params.redValue, params.redDiff) )
			redpx++;

//	qDebug() << "row : " << row << "," << lb << ":" << rb <<  " PXN = " << pxn << ", red: " << redpx;
	return redpx*100 / pxn;
}

//
int recognizeImage(const QPixmap &img, const RecogParams &params)
{
	//http://habrahabr.ru/post/136343/
	//http://habrahabr.ru/post/137868/
	//http://habrahabr.ru/post/136530/

	QImage qim = img.toImage();
	//qim.save("c:/users/anton/desktop/endoee.bmp");
	
	int startRow = qim.height() * 0.3
		, endRow = qim.height() * 0.7;

	int redpcn = 0, cnt = 0;
	for (int i = startRow; i < endRow; i += 5) {
		int leftBorder = 0, rightBorder = qim.width() - 1;
		for (int li = 0; li < qim.width(); li++)
			if (pixIsBlack(qim.pixel(li, i),params.blackValue))
				leftBorder = li;
			else
				break;
		for (int ri = qim.width() - 1; ri > 0; ri--)
			if (pixIsBlack(qim.pixel(ri, i),params.blackValue))
				rightBorder = ri;
			else
				break;
		int pcn = detectRedRow( qim, i, leftBorder+10, rightBorder-10,params );
		//qDebug() << "RED_PERCENT: " << pcn;
		cnt++;
		redpcn += pcn;
	}

	return (double)redpcn/cnt ;
}