#include "vdr_capture.h"
#include "settings.h"

#include "ProgramConfig.h"
#include "commonUtils.h"
#include "manager.h"
#include "devices.h"
#include "recognize.h"
#include "ratioimg.h"
#include "ffmpegHelper.h"

//���������� ����� ����������
//1. cd $vdviewer/vdr_capture
//2. lupdate . -ts /d/projects/vidarstudio/vdr_capture/Resources/vdr_capture_ru.ts
//3. ������� � QtLinguist, ����� ��������������

/////////////////////////////////////////////////////////////////////////////////////////
// vdr_capture //////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
vdr_capture::vdr_capture(QWidget *parent)
	: QMainWindow(parent)
	, m_prepared(false)
	, m_recording(false)
	, m_waitToQuit(false)
	, m_ffmpegProcReady(false)
	, m_tempPath(QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/vdr_capture")
	, m_serial(new SerialThread)
	, m_managerDialog(NULL)
	, m_startedByRecog(false)
	, m_videoWidget(NULL)
{
	setWindowTitle(tr("Vidar Capture"));
	m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("vdr_capture"));
	
	LOG4CPLUS_DEBUG(m_log, QString("Temp Path: " + m_tempPath).toStdString().c_str());

	ui.setupUi(this);

	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint );
	
	statusBar()->hide();

	createActions();

	loadStyles();

	auto cfg = ProgramConfig::instance().capture();
	m_currentMode = cfg->captureMode;
	m_currentMode == EVideo ? m_modeVideo->setChecked(true) : m_modeImage->setChecked(true);
	slotModeChanged(m_currentMode == EVideo ? m_modeVideo : m_modeImage);

	if (cfg->previewMode == EAlwaysShow) slotShow();

	createInfoWidget();

	m_devid = cfg->startId;
	loadDevice();

	updateGUI();

	m_reciconTimer.setInterval(700);
	connect(&m_reciconTimer, SIGNAL(timeout()), SLOT(slotIconTimer()));

	/**
	QFileInfo ffmfi(ProgramConfig::instance().getFFmpegPath());
	if (ffmfi.exists()){
		CommonUtils::removeDir(m_tempPath);
		QDir tmppath(m_tempPath);
		tmppath.mkpath(".");
	}*/

	m_rebootTimer.setInterval(cfg->findStreamMin * 60 * 1000);
	int  m = cfg->findStreamMin;
	connect(&m_rebootTimer, &QTimer::timeout, this, [this]() {
		qDebug() << "Reboot timer, m_prepared = " << QString::number(m_prepared);
		LOG4CPLUS_INFO(m_log, QString("Reboot timer, m_prepared = %1").arg(m_prepared).toStdString().c_str());
		if (m_prepared) return;
		m_rebootTimer.stop();
		slotMakeGetQuery();
	});

	connect(&m_activateTimer, SIGNAL(timeout()), SLOT(slotActivateGraph()));

	m_limitCheckTimer.setInterval(1*1000);
	connect(&m_limitCheckTimer, SIGNAL(timeout()), SLOT(slotLimitCheckTimeout()));
	m_limitCheckTimer.start();
}

//
QWidget *vdr_capture::initQtAVPlayer()
{
	QtAV::Widgets::registerRenderers();
	m_player.reset(new QtAV::AVPlayer);

	auto cfg = ProgramConfig::instance().capture();
	QWidget *videowidget;
	if (cfg->qtavwidgettype == 1)
		videowidget = new QtAV::Direct2DRenderer;
	else if (cfg->qtavwidgettype == 2)
		videowidget = new QtAV::GLWidgetRenderer2;
	else
		videowidget = new QtAV::WidgetRenderer;

	m_player->setRenderer(dynamic_cast<QtAV::VideoRenderer*>(videowidget));
	return videowidget;
}

//
void vdr_capture::slotLimitCheckTimeout()
{
	auto cfg = ProgramConfig::instance().capture();
	if (!cfg->limitSpace) return;

	QDir checkdir(cfg->videoPath);
	QStringList fllist = checkdir.entryList(QStringList() << "*." + cfg->videoExt, QDir::NoFilter, QDir::Time);

	int excess = fllist.count() - cfg->limitSpaceValue;
	if (excess > 0) {
		fllist = fllist.mid(cfg->limitSpaceValue);
		for each (QString fl in fllist){
			bool res = checkdir.remove(fl);
			QString msg = QString("File %1 removed (more than %2 files): %3.")
				.arg(fl).arg(cfg->limitSpaceValue).arg(res?"success":"fail");
			qDebug() << msg;
			LOG4CPLUS_INFO(m_log, msg.toStdString().c_str());
		}
	}
}

//
void vdr_capture::slotMakeGetQuery()
{
	qDebug() << "Get url: " << m_getUrl;
	LOG4CPLUS_INFO(m_log, QString("Get url: %1").arg(m_getUrl).toStdString().c_str());
	
	m_nam.reset( new QNetworkAccessManager() );
	QNetworkRequest req(m_getUrl);
	connect(m_nam.get(), &QNetworkAccessManager::finished, this, [=](QNetworkReply *reply) {
		QString msg = QString("Get finished, status: %1").arg(reply->error());
		qDebug() << msg;
		LOG4CPLUS_INFO(m_log, msg.toStdString().c_str());
		reply->deleteLater();
	});
	m_nam->get(req);
}

//
void vdr_capture::slotFindStreamFail()
{
	if (!m_deviceInfo.doStartGet) return;
	if (!m_rebootTimer.isActive()) m_rebootTimer.start();
}

//
void vdr_capture::connectSerialSignals()
{
	connect(m_serial.get(), SIGNAL(ctsChanged(bool)), SLOT(slotSerialCtsChanged(bool)));
	connect(m_serial.get(), SIGNAL(error(QString)), SLOT(slotSerialError(QString)));
	connect(m_serial.get(), SIGNAL(timeout(QString)), SLOT(slotSerialTimeout(QString)));
	connect(m_serial.get(), SIGNAL(connected()), SLOT(slotSerialConnected()));
}

//
void vdr_capture::createInfoWidget()
{
	m_infoLabel = new QLabel("", this, Qt::WindowStaysOnTopHint);
	m_infoLabel->setStyleSheet("QLabel{font:20px;background:#AAAAAA}");
}

//
void vdr_capture::showInfo(const QString &text, bool toshow)
{
	if (toshow){
		m_infoLabel->show();
		m_infoLabel->setText(" " + text + " ");
		m_infoLabel->adjustSize();
		m_infoLabel->move(width() / 2 - m_infoLabel->width() / 2, ui.mainToolBar->height());
	}
	else
		m_infoLabel->hide();
}

//
void vdr_capture::slotImage(const QByteArray &img)
{
	QPixmap px;
	px.loadFromData(img, "PPM");

	auto cfg = ProgramConfig::instance().capture();
	if( cfg->recogenable || cfg->recogshow )
		checkRecognize(px);

	if( !cfg->qtavwidget )
		dynamic_cast<TRatioImageWidget*>(m_videoWidget)->setPixmap(px);
}

//
void vdr_capture::checkRecognize( QPixmap &px)
{
	static bool recogstart = false, recogend = false;
	static QElapsedTimer timer;

	auto cfg = ProgramConfig::instance().capture();
	int recoginterval = cfg->recogtime * 1000;

	struct RecogParams params { cfg->black, cfg->red, cfg->reddiff, 1, 5, cfg->recogbound, cfg->recogendbound };
	int recogres = recognizeImage(px, params);
	qDebug() << "RECOG: " << recogres;
	LOG4CPLUS_INFO(m_log, QString("RECOG: %1").arg(recogres).toStdString().c_str());

	if (cfg->recogshow) {
		QPainter paint(&px);
		paint.setPen(Qt::white);
		paint.drawText(QPoint(10, 10), QString("Recog: %1").arg(recogres));
	}

	if (cfg->recogdump) {
		QString path = m_tempPath + QDateTime::currentDateTime().toString("/hhmmss-recog-%1.png");
		px.save(path.arg(recogres));
	}

	if (!cfg->recogenable) return;

	auto timerRecog = [&](auto &flag, int bound, auto action, auto comp) {
		if (comp(recogres, bound)) {
			if (!flag) {
				flag = true;
				timer.restart();
			}
			if (timer.elapsed() > recoginterval) {
				(this->*action)();
				flag = false;
				return true;
			}
		}
		else
			flag = false;
		return false;
	};

	if (m_recording) {
		if (m_startedByRecog)
			timerRecog(recogend, cfg->recogendbound, &vdr_capture::slotStopRecord, std::less<int>());
	}
	else {
		if( timerRecog(recogstart, cfg->recogbound, &vdr_capture::slotStartRecord, std::greater<int>()) )
			m_startedByRecog = true;
	}
}

//
void vdr_capture::loadStyles()
{
	QString style;

	style += tr("QWidget { font: %1px; } QLabel { font: %1px; } QString { font: %1px; }")
		.arg(20);

	style += tr("QMenu::item { font: bold %1px; } QMenu {font: bold %1px; }")
		.arg(14);
	style += tr("QMenuBar { font: bold %1px; background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 lightgray, stop:1 darkgray); background:#000000; color:#FFFFFF; }")
		.arg(14);
	style += tr("QMenuBar::item {spacing: 3px; padding: 1px 4px;background: transparent; border-radius: 4px;} ");
	style += tr("QMenuBar::item:selected { background: #a8a8a8;} ");
	style += tr("QMenuBar::item:pressed { background: #888888;} ");

	style += tr("QCheckBox::indicator:checked { image: url(:/resources/cb_checked.png); } ");
	style += tr("QCheckBox::indicator:unchecked { image: url(:/resources/cb_unchecked.png); } ");

	style += tr("QToolTip  { font: %1px; color: #CCCCCC; background: #333333; border: 1px solid gray; padding: 3px; border-radius: 5px; }")
		.arg(14);

	style += tr("QToolBar { background-color: #000000; border:0px; } ");
	style += tr("QToolButton:!hover { border:0px; padding:2px; }");
	style += tr("QToolButton:checked { background-color: #FFFFFF; padding: 2px; border:0px; }");
	style += tr("QToolButton:pressed { background-color: grey; padding: 2px; border:0px; }");
	style += tr("QToolButton:disabled{color:#888888;}");
	style += tr("QToolButton{ color: white; } ");
	style += tr("QToolButton::menu-indicator { left: -1px; top: 1px; }");
	style += tr("QToolButton::hover { background-color: grey; padding: 2px; border:0px; }");

	style += tr("QStatusBar { background-color: #000000; } QStatusBar::item {border: none; } ");
	
	((QApplication*)parent())->setStyleSheet(style);
}

//
void vdr_capture::slotSerialCtsChanged(bool state)
{
	if (!m_prepared)
		return;

	if (m_currentMode == EImage){
		slotSnapshot();
		return;
	}
	
	if ((ProgramConfig::instance().capture()->controlMode == EClickClick && !m_recording)
		|| (ProgramConfig::instance().capture()->controlMode == EHoldRecord && state))
		slotStartRecord();
	else
		slotStopRecord();
}

//
void vdr_capture::slotSerialError(QString msg)
{
	qDebug() << "Serial error " + msg;
	LOG4CPLUS_ERROR(m_log, QString("Serial error "+msg).toStdString().c_str());
}

//
void vdr_capture::slotSerialTimeout(QString msg)
{
	qDebug() << "Serial timeout " + msg;
	LOG4CPLUS_WARN(m_log, QString("Serial timeout " + msg).toStdString().c_str());
}

//
void vdr_capture::slotSerialConnected()
{
	QString msg = "Serial " + m_serial->myPortName() + " connected.";
	qDebug() << msg;
	LOG4CPLUS_DEBUG(m_log, msg.toStdString().c_str());
}


//
void vdr_capture::slotActivateGraph()
{
	if (!m_prepared )
		m_graph->start();
}

//
void vdr_capture::slotGraphStarted()
{
	m_prepared = true;
	updateGUI();
}

//
void vdr_capture::slotGraphStopped()
{
	m_prepared = false;
	m_recording = false;
	updateGUI();
	if (m_waitToQuit)
		QApplication::quit();
}

//
void vdr_capture::slotGraphRecordBegin()
{
	qDebug() << "slotGraphRecordBegin";
	m_recording = true;
	updateGUI();
	m_reciconTimer.start();
	QMetaObject::invokeMethod(m_serial.get(), "slotBlinkFilmBegin");
	auto cfg = ProgramConfig::instance().capture();
	if (isHidden() && cfg->previewMode == EShowOnRecord)
		slotShow();
}

//
void vdr_capture::slotGraphRecordEnd(QString path)
{
	qDebug() << "slotGraphRecord END";
	m_recording = false;
	m_startedByRecog = false;
	updateGUI();
	m_reciconTimer.stop();
	m_tray->setIcon(QIcon(":/resources/video_on"));

	QMetaObject::invokeMethod(m_serial.get(), "slotBlinkFilmEnd");

	auto cfg = ProgramConfig::instance().capture();
	if (!isHidden() && cfg->previewMode == EShowOnRecord)
		slotShow();

	/*if (m_ffmpegProcReady){
	auto info = FFmpegHelper::videoInfo(path);
	if (info.contains("duration") && info["duration"].toInt() < cfg->filmMinDurationSec) {
		QFile fl(path);fl.remove();
		qDebug() << "Removed small file " << path << " , duration: " << info["duration"].toString();
		LOG4CPLUS_DEBUG(m_log, QString("Removed small file %1, duration: %2").arg(path).arg(info["duration"].toString()).toStdString().c_str());
	}

	if (m_ffmpegProcReady){
		QString ffmpegPath = ProgramConfig::instance().getFFmpegPath();

		QFileInfo flin(path);
		QDir dir(ProgramConfig::instance().capture()->videoPath);
		QString newfl = dir.absoluteFilePath(flin.fileName());

		QString args = "\"%1\" -i \"%2\" " + m_deviceInfo.ffmpegArgs + " -y \"%3\"";
		QString ffmpegTotal = args.arg(ffmpegPath).arg(path).arg(newfl);
		LOG4CPLUS_DEBUG(m_log, QObject::tr("postProc: ffmpegTotal= <%1>").arg(ffmpegTotal).toStdString().c_str());

		auto pr = new QProcess(this);
		pr->start(ffmpegTotal);
	}*/
}

//
void vdr_capture::slotGraphSnapshotTaken()
{
	showInfo(tr("SNAPSHOT TAKEN"), true);
	QTimer::singleShot(2000, this, SLOT(updateGUI()));
	QMetaObject::invokeMethod(m_serial.get(), "slotBlinkImage");
}

//
QString vdr_capture::newVideoPath()
{
	auto cfg = ProgramConfig::instance().capture();
	QString prefix = cfg->useFilePrefix ? cfg->filePrefix : "";
	QDir dir(m_ffmpegProcReady ? m_tempPath : ProgramConfig::instance().capture()->videoPath);
	LOG4CPLUS_DEBUG(m_log, QString("Videodir: " + dir.absolutePath()).toStdString().c_str());
	return dir.absoluteFilePath(prefix + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss-z"));
}

//
QString vdr_capture::newSnapshotPath()
{
	auto cfg = ProgramConfig::instance().capture();
	QString prefix = cfg->useFilePrefix ? cfg->filePrefix : "";
	QDir dir(ProgramConfig::instance().capture()->imagePath);
	LOG4CPLUS_DEBUG(m_log, QString("Imagedir: " + dir.absolutePath()).toStdString().c_str());
	return dir.absoluteFilePath(prefix + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss-z")
		+ "." + ProgramConfig::instance().capture()->imageExt);
}

//
void vdr_capture::slotTrayActivated(QSystemTrayIcon::ActivationReason reason)
{
	if (reason == QSystemTrayIcon::DoubleClick)
		slotShow();
}

//
void vdr_capture::slotIconTimer()
{
	static bool turn = false;
	m_tray->setIcon(QIcon(turn ? ":/resources/video_on" : ":/resources/video_work"));
	turn = !turn;
}

//
void vdr_capture::createActions()
{
	m_tray = new QSystemTrayIcon;
	m_tray->setIcon(QIcon(":/resources/video_off"));

	QMenu *sm = new QMenu;
	m_tray->setContextMenu(sm);

	m_start = sm->addAction(QIcon(":/resources/record"),tr("Start"));
	connect(m_start, SIGNAL(triggered()), SLOT(slotStartRecord()));
	m_stop = sm->addAction(QIcon(":/resources/stop"),tr("Stop"));
	connect(m_stop, SIGNAL(triggered()), SLOT(slotStopRecord()));
	m_snap = new QAction(QIcon(":/resources/snapshot"),tr("Snapshot"),NULL);
	m_snapTray = sm->addAction(QIcon(":/resources/snapBlack"), tr("Snapshot"));
	connect(m_snap, SIGNAL(triggered()), SLOT(slotSnapshot()));
	connect(m_snapTray, SIGNAL(triggered()), SLOT(slotSnapshot()));
	m_show = sm->addAction(tr("Show"));
	connect(m_show, SIGNAL(triggered()), SLOT(slotShow()));
	connect(m_tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(slotTrayActivated(QSystemTrayIcon::ActivationReason)));

	m_modeMenu = new QMenu;
	m_modeVideo = m_modeMenu->addAction(tr("Video"));
	m_modeVideo->setCheckable(true);
	m_modeImage = m_modeMenu->addAction(tr("Image"));
	m_modeImage->setCheckable(true);
	m_modeGroup = new QActionGroup(this);
	m_modeGroup->addAction(m_modeVideo);
	m_modeGroup->addAction(m_modeImage);
	connect(m_modeGroup, SIGNAL(triggered(QAction*)), SLOT(slotModeChanged(QAction*)));

	sm->addMenu(m_modeMenu);

	m_settingsAct = sm->addAction(QIcon(":/resources/settings.png"), tr("Settings"));
	connect(m_settingsAct, SIGNAL(triggered()), SLOT(slotSettings()));

	m_close = sm->addAction(tr("Exit"));
	QObject::connect(m_close, SIGNAL(triggered()), SLOT(slotClose()));

//	m_manager = sm->addAction(tr("Manager"));
//	QObject::connect(m_manager, SIGNAL(triggered()), SLOT(slotManager()));

	m_tray->show();

	QWidget *empty = new QWidget;
	empty->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

	ui.mainToolBar->addAction(m_start);
	ui.mainToolBar->addAction(m_stop);
	ui.mainToolBar->addAction(m_snap);
	ui.mainToolBar->addWidget(empty);
	ui.mainToolBar->addAction(m_settingsAct);
	m_modeMenuButton = new QToolButton;
	m_modeMenuButton->setMenu(m_modeMenu);
	m_modeMenuButton->setPopupMode(QToolButton::InstantPopup);
	ui.mainToolBar->addWidget(m_modeMenuButton);
	ui.mainToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}

//
void vdr_capture::updateGUI()
{
	m_start->setEnabled(m_prepared && !m_recording);
	m_stop->setEnabled(m_prepared && m_recording);
	m_snap->setEnabled(m_prepared);
	m_snapTray->setEnabled(m_prepared);

	m_close->setEnabled( true );

	m_tray->setIcon(QIcon(m_prepared ? ":/resources/video_on" : ":/resources/video_off"));
	setWindowIcon(QIcon(m_prepared ? ":/resources/video_on" : ":/resources/video_off"));
	
	m_tray->setToolTip(m_deviceInfo.name);
	setWindowTitle(tr("Vidar Capture") + " - " + m_deviceInfo.name);

	m_show->setText(isHidden() ? tr("Show") : tr("Hide"));
	
	auto cfg = ProgramConfig::instance().capture();
	
	if (m_recording)
		showInfo(tr("RECORDING"), m_recording);
	else if (!m_prepared){
		showInfo(tr("CONNECTING.."), !m_prepared);
		if( !cfg->qtavwidget )
			dynamic_cast<TRatioImageWidget*>(m_videoWidget)->setPixmap(QPixmap());
	}
	else
		showInfo("",false);

	m_settingsAct->setEnabled(cfg->deviceInfo.count());
}

//
void vdr_capture::slotSettings()
{
	SettingsDialog dialog(m_profileId, m_devid, this);
	dialog.restoreGeometry(ProgramConfig::instance().capture()->settingsGeometry);
	connect(&dialog, SIGNAL(showDeviceList()),SLOT(slotManager()));

	if (dialog.exec())
		loadDevice();
}

//
QString vdr_capture::constructUrl()
{
	if (!m_deviceInfo.stdSource)
		return m_deviceInfo.urls[m_deviceInfo.sourceMode];

	QString url = "rtsp://";
	if (m_deviceInfo.useAuth)
		url += m_deviceInfo.login + ":" + m_deviceInfo.password + "@";
	url += m_deviceInfo.ip + ":" + QString::number(Encoders::encoderInfo(m_deviceInfo.stdSourceId).port);
	
	//����� ��������� � ffmpegworker ��� ����-�� ����
	url += Encoders::encoderInfo(m_deviceInfo.stdSourceId).urlSuffix;
	return url;
}

//
void vdr_capture::loadDevice()
{
	auto cfg = ProgramConfig::instance().capture();
	
	if (m_videoWidget) {
		delete m_videoWidget;
		m_player.reset();
	}
	if (cfg->qtavwidget)
		m_videoWidget = initQtAVPlayer();
	else {
		m_videoWidget = new TRatioImageWidget;
		m_videoWidget->setStyleSheet("QWidget{background:#000000;}");
	}

	setCentralWidget(m_videoWidget);
	
	if (!m_devid.isEmpty() && !cfg->deviceInfo.contains(m_devid)) {
		LOG4CPLUS_ERROR(m_log, QString("Empty or wrong startId: " + m_devid).toStdString().c_str());
		return;
	}

	if (m_devid.isEmpty()) {
		m_devid = QDateTime::currentDateTime().toString("zzzssmmddMMyyyy");
		m_deviceInfo = makeDefaultDevice( m_devid );
		cfg->deviceInfo[m_devid] = m_deviceInfo;
		cfg->startId = m_devid;
	}
	else
		m_deviceInfo = cfg->deviceInfo[m_devid];

	m_currentMode = cfg->captureMode;
	m_currentMode == EVideo ? m_modeVideo->setChecked(true) : m_modeImage->setChecked(true);
	slotModeChanged(m_currentMode == EVideo ? m_modeVideo : m_modeImage); 
	
	m_activateTimer.stop();
	
	m_getUrl = m_deviceInfo.getUrl;
	m_ffmpegProcReady = m_deviceInfo.postProcessing;

	initGraph();

	if (cfg->qtavwidget)
		loadQtAVPlayer(); 
	
	m_serial.reset(new SerialThread);
	m_serial->startSlave(m_deviceInfo.serialPort, 100, m_deviceInfo.blinkTimeout, m_deviceInfo.blinkOn);
	connectSerialSignals();

	updateGUI();

	QMetaObject::invokeMethod(m_managerDialog, "updateDevices");

	m_activateTimer.start(10000);
	QTimer::singleShot(500, this, SLOT(slotActivateGraph()));
}

//
void vdr_capture::loadQtAVPlayer()
{
	auto cfg = ProgramConfig::instance().capture();
	
	QDir dir(cfg->imagePath);
	auto *qtcapture = m_player->videoCapture();
	qtcapture->setAutoSave();
	qtcapture->setSaveFormat(cfg->imageExt);
	qtcapture->setCaptureDir(dir.absolutePath());
	
	QVariantHash opt;
	if (Encoders::encoderInfo(m_deviceInfo.stdSourceId).useTcpTransport) {
		opt["rtsp_transport"] = "tcp";
		m_player->setOptionsForFormat(opt);
	}

	m_player->setFile(constructUrl());
	m_player->play();
}

//
void vdr_capture::initGraph()
{
	auto cfg = ProgramConfig::instance().capture();
	auto grf = TGraphFactory::instance().graph(m_deviceInfo.sourceMode);
	bool needconnect = true;
	if (grf.get() != NULL) {
		needconnect = (m_graph != grf);
		m_graph = grf;
		m_graph->setWindow((HWND)m_videoWidget->winId());
	}
	else {
		LOG4CPLUS_ERROR(m_log, QString("grf.get() == NULL").toStdString().c_str());
		return;
	}

	m_graph->stop();

	m_graph->setUrl(constructUrl());
	m_graph->setRestartTimeout(cfg->filmRestartMinutes);
	m_graph->setDeviceInfo(m_deviceInfo);

	if (needconnect) {
		connect(m_graph.get(), SIGNAL(imagePPM(const QByteArray&)), SLOT(slotImage(const QByteArray&)));
		connect(m_graph.get(), SIGNAL(findStreamFail()), SLOT(slotFindStreamFail()));
		connect(m_graph.get(), SIGNAL(started()), SLOT(slotGraphStarted()));
		connect(m_graph.get(), SIGNAL(stopped()), SLOT(slotGraphStopped()));
		connect(m_graph.get(), SIGNAL(recordBegin()), SLOT(slotGraphRecordBegin()));
		connect(m_graph.get(), SIGNAL(recordEnd(QString)), SLOT(slotGraphRecordEnd(QString)));
		connect(m_graph.get(), SIGNAL(snapshotTaken()), SLOT(slotGraphSnapshotTaken()));
	}
}

//
void vdr_capture::slotModeChanged(QAction *action)
{
	QString title = QString(tr("Mode: %1")).arg(action->text());
	m_modeMenu->setTitle( title );
	m_modeMenuButton->setText( title );

	m_currentMode = action == m_modeVideo ? EVideo : EImage;
}

//
void vdr_capture::slotStartRecord()
{
	m_graph->beginVideoRecord(newVideoPath());
}

//
void vdr_capture::slotStopRecord()
{
	m_graph->endVideoRecord();
}

//
void vdr_capture::slotSnapshot()
{
	auto cfg = ProgramConfig::instance().capture();
	if (cfg->qtavwidget) {
		QString prefix = cfg->useFilePrefix ? cfg->filePrefix : "";
		auto *qtcapture = m_player->videoCapture();
		qtcapture->setCaptureName(prefix + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss-z"));
		qtcapture->capture();
	}
	else
		m_graph->takeSnapshot(newSnapshotPath());
}

//
void vdr_capture::closeEvent(QCloseEvent* event)
{
	slotShow();
	event->ignore();
}

//
bool vdr_capture::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
	MSG *msg = static_cast<MSG*>(message);
	if (msg->message == WM_QUERYENDSESSION) {
		qDebug() << "WM_QUERYENDSESSION";
		LOG4CPLUS_WARN(m_log, QString("Shutdown query.").toStdString().c_str());
		*result = true;
		return true;
	}
	if (msg->message == WM_ENDSESSION) {
		qDebug() << "WN_ENDSESSION";
		LOG4CPLUS_WARN(m_log, QString("Shutdown close.").toStdString().c_str());
		slotClose();
		QMutex *mutex = m_graph->mutex();
		if (mutex->tryLock(5000))
			mutex->unlock();
		*result = true;
		return true;
	}
	return false;
}

//
void vdr_capture::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape && isVisible())
		slotShow();
}

//
void vdr_capture::slotShow()
{
	if (isHidden()){
		m_show->setText(tr("Hide"));
		show();

		static bool restored = false;
		if (!restored){
			restoreGeometry(ProgramConfig::instance().capture()->mainGeometry);
			restored = true;
		}
	}
	else{
		m_show->setText(tr("Show"));
		hide();
	}
}

//
void vdr_capture::slotClose()
{
	if (m_graph.get()){
		m_graph->stop();
		m_waitToQuit = true;
	}
	else
		QApplication::quit();
}

//
vdr_capture::~vdr_capture()
{
	delete m_tray;
}

//
void vdr_capture::slotManager()
{
	if (m_managerDialog == NULL) {
		m_managerDialog = new ManagerDialog(m_profileId, m_devid, this);
		connect(m_managerDialog, SIGNAL(activateDevice(const QString&)), SLOT(slotActivateDevice(const QString&)));
	}
	if (m_managerDialog->isVisible())
		return;
	m_managerDialog->exec();
}

//
void vdr_capture::slotActivateDevice( const QString &devid)
{
	m_devid = devid;
	loadDevice();
}

