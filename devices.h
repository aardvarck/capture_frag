#pragma once

#include "ConfigCapture.h"

struct EncoderInfo
{
	QString name;
	QString urlSuffix;
	bool useTcpTransport;
	int port;
};

/////////////////////////////////////////////////////////////////////////////////////////
// Encoders /////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class Encoders
{
public:
	static const EncoderInfo &encoderInfo(const QString &name) {
		static EncoderInfo wrongEnc{ "wrong_name","/wrong_name_sfx", false };
		if (!encoders().contains(name)) {
			qDebug() << "EncoderInfo &encoderInfo:: unknown encoder name " << name;
			return wrongEnc;
		}
		return encoders()[name];
	}
	static QStringList encodersList() { return encoders().keys();  }

private:
	static QMap<QString, EncoderInfo>& encoders() {
		static QMap<QString, EncoderInfo> sencoders; 
		sencoders["Axis"] = EncoderInfo{ "Axis", "/axis-media/media.amp", true, 554 };
		sencoders["Kiloview"] = EncoderInfo{ "Kiloview", "/ch01", false, 554 };
		sencoders["Oupree"] = EncoderInfo{ "Oupree", "/sdi", true, 554 };
		sencoders["Rvi"] = EncoderInfo{ "Rvi", "/cam/realmonitor?channel=1&subtype=0", false, 554 };
		return sencoders;
	}
};

//
DeviceInfo makeDefaultDevice(const QString &id);