#pragma once

#include "log4cplus.h"
#include "ConfigCapture.h"

struct AVFormatContext;
struct AVCodecContext;
struct SwsContext;
struct AVStream;
struct AVFrame;

////////////////////////////////////////////////////////
// TFFMpegWorker ///////////////////////////////////////
////////////////////////////////////////////////////////
//
struct CtxStreamInfo
{
	AVFormatContext *ctx;
	AVStream *stream;
	int pts;
	QString file;
	bool validOut;
};

class TFFMpegWorker : public QObject
{
	Q_OBJECT
public:
	TFFMpegWorker();
	~TFFMpegWorker();
	//
	void setDeviceInfo(const DeviceInfo &devinfo);
	QMutex *mutex() { return &m_mutex; }

public slots:
	void slotStart(QString url, int w, int h);
	void slotBeginRecord(QString filepath);
	void slotEndRecord();
	void slotSnapshot(QString filepath);
	void slotFinish();

signals:
	void image( char *image, int width, int height );
	void killMe();
	void prepared(bool _prepared);
	void started();
	void stopped();
	void connected(bool _connected);
	void error(QString msg);
	void startRecord();
	void recorded();
	void findStreamFail();
	
private:
	bool initSource();
	void prepareFramesInfo();
	void beginRecord( CtxStreamInfo &outCtx, bool ordRecord = true );
	void endRecord( CtxStreamInfo &outCtx, bool ordRecord = true );
	void makeSnapshot(AVFrame *frame);
	//
	AVFormatContext *m_inFormatCtx;
	AVCodecContext *m_inCodecCtx, *m_codecCtx;
	SwsContext *m_swsPreviewCtx;
	CtxStreamInfo m_permRecCtx, m_OrdRecCtx;
	AVStream *m_inStream;
	AVFrame *m_frame, *m_frameRGB;
	int m_videoIdx, m_previewWidth, m_previewHeight, m_keyInterval;
	bool m_record, m_finish, m_snapshot, m_goingtostart;
	log4cplus::Logger m_log;
	DeviceInfo m_devInfo;
	std::unique_ptr<QTimer> m_permTimer;
	QString m_url, m_snapPath;
	QMutex m_mutex;
	uint8_t *m_buffer;
};