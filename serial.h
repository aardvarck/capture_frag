#pragma once

/////////////////////////////////////////////////////////////////////////////////////////
// SerialThread /////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class SerialThread : public QThread
{
    Q_OBJECT

public:
	SerialThread(QObject *parent = 0);
	~SerialThread();

    void startSlave(const QString &portName, int waitTimeout, int bounceTime, bool blink);
    void run();
	QString myPortName() { return portName; };

signals:
    void ctsChanged(bool state);
	void connected();
    void error(QString s);
    void timeout(const QString &s);

public slots:
	void slotBlinkFilmBegin();
	void slotBlinkFilmEnd();
	void slotBlinkImage();

private slots:
	void slotTimeout();
	void slotSetDTR(bool state);
	void slotBlinkTimeout();

private:
	log4cplus::Logger m_log; 
	QSerialPort *m_serial;
	QString portName;
    int waitTimeout;
    QMutex mutex;
    bool quit, m_ctsState, m_stateChanged, m_opened;
	int m_bounceTimeout;
	QTime m_lastBounceTime;
	QTimer m_blinkTimer;
	bool m_blinking, m_blinkState, m_blinkOn;
};
