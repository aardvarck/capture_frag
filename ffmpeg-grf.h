#include "capture_graph.h"

class TFFMpegWorker;
/////////////////////////////////////////////////////////////////////////////////////////
// TFFMpegGrf ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
class TFFMpegGrf : public TCaptureGraph
{
	Q_OBJECT
public:
	TFFMpegGrf(const QString &name);
	~TFFMpegGrf();
	//
	virtual void start();
	virtual void takeSnapshot(const QString &path);
	virtual void stop();
	virtual QString hint();

protected:
	virtual void beginRecord(const QString &path);
	virtual void endRecord();

signals:
	void imagePPM(const QByteArray &img);
	void findStreamFail();

private slots:
	void slotWorkerError(QString msg);
	void slotWorkerFinished(QString msg);
	void slotFrame(char *frame, int width, int height);
	void slotPrepared(bool prepared);
	void slotConnected(bool _connected);
	void deleteLaterTest();

private:
	QString m_lastPath;
	bool m_rtspOpened, m_workerStarted;
	TFFMpegWorker *m_worker;
	QThread m_workerThread;
};