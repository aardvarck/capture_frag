#include "ffmpeg-grf.h"
#include "ffmpegWorker.h"
#include "ProgramConfig.h"

/////////////////////////////////////////////////////////////////////////////////////////
// TFFMpegGrf ///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
TFFMpegGrf::TFFMpegGrf(const QString &name)
	: TCaptureGraph(name)
	, m_rtspOpened(false)
	, m_workerStarted(false)
{
	m_log = log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("TFFMpegGrf"));

	m_worker = new TFFMpegWorker;
	m_worker->moveToThread(&m_workerThread);
	m_mutex = m_worker->mutex();
	connect(&m_workerThread, SIGNAL(finished()), m_worker, SLOT(deleteLater()));
	connect(&m_workerThread, SIGNAL(finished()), SLOT(deleteLaterTest()));
	connect(m_worker, SIGNAL(killMe()), &m_workerThread, SLOT(quit()));
	connect(m_worker, SIGNAL(prepared(bool)), SLOT(slotPrepared(bool)));
	connect(m_worker, SIGNAL(connected(bool)), SLOT(slotConnected(bool)));
	connect(m_worker, SIGNAL(image(char*,int,int)), SLOT(slotFrame(char*,int,int)));
	connect(m_worker, SIGNAL(startRecord()), SIGNAL(recordBegin()));
	connect(m_worker, &TFFMpegWorker::recorded, this, [&]{emit recordEnd(m_lastPath);});
	connect(m_worker, SIGNAL(findStreamFail()), SIGNAL(findStreamFail()));
	connect(m_worker, &TFFMpegWorker::started, this, [&] {m_workerStarted = true; });
	connect(m_worker, &TFFMpegWorker::stopped, this, [&] {m_workerStarted = false; });
}

void TFFMpegGrf::deleteLaterTest()
{
	int g = 0;
	g++;
}

//
TFFMpegGrf::~TFFMpegGrf()
{
	QMetaObject::invokeMethod(m_worker, "slotFinish");
	m_workerThread.quit();
	m_workerThread.wait();
}

//
QString TFFMpegGrf::hint()
{
	return tr(	"OPR-NH100:  rtsp://x.x.x.x:554/sdi\n"
				"KV-EC101A:  rtsp://x.x.x.x:554/ch01\n"
				"AXIS M7001:  rtsp://x.x.x.x:554/axis-media/media.amp\n"
				"RVI 125A:  rtsp://x.x.x.x:554/cam/realmonitor?channel=0&subtype=0");
}

//
void TFFMpegGrf::start()
{
	if (m_workerStarted)
		return;
	
	m_worker->setDeviceInfo(m_devInfo);
	m_workerThread.start();

	LOG4CPLUS_DEBUG(m_log, QString("Starting worker.").toStdString().c_str());
	int mult = 2;
	m_previewWidth = 480*mult;
	m_previewHeight = 270*mult;
	QMetaObject::invokeMethod(m_worker, "slotStart", Q_ARG(QString,m_url), Q_ARG(int,m_previewWidth), Q_ARG(int,m_previewHeight));
}

//
void TFFMpegGrf::slotPrepared(bool prepared)
{
	LOG4CPLUS_DEBUG(m_log, QString("Prepared state %1").arg(prepared).toStdString().c_str());
	if (prepared)
		emit started();
	else
		emit stopped();
	m_rtspOpened = prepared;
}

//
void TFFMpegGrf::slotConnected(bool _connected)
{
	LOG4CPLUS_DEBUG(m_log, QString("Connected state %1").arg(_connected).toStdString().c_str());
//	emit connected();
}

//
void TFFMpegGrf::beginRecord(const QString &path)
{
	auto cfg = ProgramConfig::instance().capture();
	m_lastPath = path + "." + cfg->videoExt;
	LOG4CPLUS_DEBUG(m_log, QString("Begin record " + m_lastPath).toStdString().c_str());
	QMetaObject::invokeMethod(m_worker, "slotBeginRecord", Q_ARG(QString, m_lastPath));
}

//
void TFFMpegGrf::endRecord()
{
	LOG4CPLUS_DEBUG(m_log, QString("End record.").toStdString().c_str());
	QMetaObject::invokeMethod(m_worker, "slotEndRecord");
}

//
void TFFMpegGrf::takeSnapshot(const QString &path)
{
	LOG4CPLUS_DEBUG(m_log, QString("Snapshot " + path).toStdString().c_str());
	QMetaObject::invokeMethod(m_worker, "slotSnapshot", Q_ARG(QString, path));
	emit snapshotTaken();
}

//
void TFFMpegGrf::stop()
{
	LOG4CPLUS_DEBUG(m_log, QString("TFFMpegGrf::stop()").toStdString().c_str());
	QMetaObject::invokeMethod(m_worker, "slotFinish");
}

//
void TFFMpegGrf::slotWorkerError(QString msg)
{
	LOG4CPLUS_DEBUG(m_log, QString("Worker error: " + msg).toStdString().c_str());
}

//
void TFFMpegGrf::slotWorkerFinished(QString msg)
{
	LOG4CPLUS_DEBUG(m_log, QString("Worker finished: " + msg).toStdString().c_str());
	qDebug() << "End";
}

//
void TFFMpegGrf::slotFrame(char *frame, int width, int height)
{
	auto imgsize = width*height * 3;

	QString str("P6\n%1 %2\n255\n");
	str = str.arg(width).arg(height);
	auto ba2 = str.toLatin1();
	const char *strch = ba2.constData();

	QByteArray ba;
	ba.append(strch, strlen(strch));
	ba.append(frame, width*height * 3);

	emit imagePPM(ba);

	delete frame;
}