#include "dxgraph.h"
#include "dxutils.h"

#ifdef USE_DIRECTX_CAPTURE
#ifndef NDEBUG
#pragma comment (lib, "strmbasd.lib")
#else
#pragma comment (lib, "strmbase.lib")
#endif

#define DX_WINDOWLESS

/////////////////////////////////////////////////////////////////////////////////////////
// TDXGraph /////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//
TDXGraph::TDXGraph(const QString &name)
	: TCaptureGraph(name)
{
	::CoInitialize(NULL);

	HRESULT hr = ::CoCreateInstance(CLSID_FilterGraph, 0
					, CLSCTX_INPROC_SERVER,	IID_IGraphBuilder, (void**)&m_graph); 
}

//
TDXGraph::~TDXGraph()
{
	HRESULT hr;
	OAFilterState fs;

	hr = getMediaControl()->GetState(2000, &fs);
	if (!SUCCEEDED(hr))
		return;

	if ( fs != State_Stopped )
		hr = getMediaControl()->Stop();
}

//
void TDXGraph::start()
{
	HRESULT hr;

#ifdef RTSP_VIDEO
	hr = ::CoCreateInstance(CLSID_AxisRTPSrc, NULL, CLSCTX_INPROC, IID_IBaseFilter, (void**)&m_rtsp);
	hr = ::CoCreateInstance(CLSID_AVIDec, NULL, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void **)&m_avidec);
	hr = ::CoCreateInstance(CLSID_VideoMixingRenderer9, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_render);
	hr = ::CoCreateInstance(CLSID_InfTee, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_tee);
	hr = ::CoCreateInstance(CLSID_Matroska, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_matroska);
	SetProperty(m_matroska, "FileType", 1);
#else
	m_capture = CreateFilterByName( L"Dazzle DVC100 Video", CLSID_WDMCapDev );
//	hr = ::CoCreateInstance(CLSID_Dazzle, NULL, CLSCTX_INPROC, IID_IBaseFilter, (void**)&m_capture);
	hr = ::CoCreateInstance(CLSID_InfTee, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_tee);
//	hr = ::CoCreateInstance(CLSID_VideoRenderer, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_render);
	hr = ::CoCreateInstance(CLSID_VideoMixingRenderer9, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_render);
	hr = ::CoCreateInstance(CLSID_x264, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_x264);
	hr = ::CoCreateInstance(CLSID_AviMux, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_avimux);
	hr = ::CoCreateInstance(CLSID_FileWriter, 0, CLSCTX_INPROC_SERVER, IID_IBaseFilter, (void**)&m_file);
#endif

	hr = m_graph->AddFilter(m_capture, L"capture");
	hr = m_graph->AddFilter(m_tee, L"tee");
	hr = m_graph->AddFilter(m_render, L"renderer");
//	hr = m_graph->AddFilter(m_x264, L"x264");
//	hr = m_graph->AddFilter(m_avimux, L"avimux");
//	hr = m_graph->AddFilter(m_file, L"file");

	registerGraphAsPublic(m_graph); 
	
	/**
	if (!m_rtspOpened){
		hr = LoadFileSource(m_rtsp, m_url);
		if (!SUCCEEDED(hr)){
			qDebug() << "load file source failed with " << m_url << " err  " << QString::number(hr, 16);
			return false;
		}
		m_rtspOpened = true;
	}
	/**/

	hr = buildGraph();
	if (!SUCCEEDED(hr)){
		qDebug() << "buildGraph failed" << " err  " << QString::number(hr, 16);
		return;
	}

	//!!!!m_isPrepared = true;
	//emit stateChanged(ERunning);

	return;
}

//
HRESULT TDXGraph::disconnectAllPins()
{
	CComPtr<IEnumFilters> fEnum;
	CComPtr<IBaseFilter> filter;
	m_graph->EnumFilters(&fEnum);
	while (fEnum->Next(1, &filter, 0) == S_OK)
	{
		CComPtr<IEnumPins>	pEnum;
		CComPtr<IPin>		pPin;
		filter->EnumPins(&pEnum);
		while (pEnum->Next(1, &pPin, 0) == S_OK){
			m_graph->Disconnect(pPin);
			pPin.Release();
		}
		filter.Release();
	}
	return S_OK;
}

//
HRESULT TDXGraph::prepareFileOutput()
{
	OAFilterState fs;
	HRESULT hr;
	hr = getMediaControl()->GetState(2000, &fs);
	if (!SUCCEEDED(hr))
		return true;

	if (fs == State_Stopped){
		CComPtr<IPin> teeOut, matroskaIn;
		teeOut = GetPin(m_tee, PINDIR_OUTPUT, false);
		matroskaIn = GetPin(m_matroska, PINDIR_INPUT, false);
		hr = m_graph->Disconnect(teeOut);
		hr = m_graph->Disconnect(matroskaIn);
		hr = m_graph->Connect(teeOut, matroskaIn);

		CComPtr<IPin> matroskaout, filein;
		matroskaout = GetPin(m_matroska, PINDIR_OUTPUT, false);
		filein = GetPin(m_file, PINDIR_INPUT, false);

		hr = SetFileName(matroskaout, m_file, m_vp);

		hr = m_graph->Connect(matroskaout, filein);

		return S_OK;
	}
	return hr;
}

//
CComPtr<IMediaControl> TDXGraph::getMediaControl()
{
	HRESULT			hr;
	CComPtr<IMediaControl>	mc;

	hr = m_graph->QueryInterface(IID_IMediaControl, (void**)&mc);
	if (hr != S_OK)
		return NULL;

	return mc;
}

//
HRESULT TDXGraph::buildGraph()
{
	HRESULT hr;
	DWORD err;
	int code;

	CComPtr<IPin> capOut, teeIn;
	capOut = GetPin( m_capture, PINDIR_OUTPUT );
	teeIn = GetPin( m_tee, PINDIR_INPUT );
	hr = m_graph->Connect(capOut, teeIn);
	if (!SUCCEEDED(hr)) return hr;

	/**
	CComPtr<IPin> teeOutCap, x264In;
	teeOutCap = GetPin(m_tee, PINDIR_OUTPUT);
	x264In = GetPin(m_x264, PINDIR_INPUT);
	hr = m_graph->Connect(teeOutCap, x264In);
	if (!SUCCEEDED(hr)) return hr;

	CComPtr<IPin> x264Out, muxIn;
	x264Out = GetPin(m_x264, PINDIR_OUTPUT);
	muxIn = GetPin(m_avimux, PINDIR_INPUT);
	hr = m_graph->Connect(x264Out, muxIn);
	if (!SUCCEEDED(hr)) return hr;
	/**/

	CComPtr<IPin> teeOutPre, renIn;
	teeOutPre = GetPin( m_tee, PINDIR_OUTPUT );
	renIn = GetPin( m_render, PINDIR_INPUT );
	hr = m_graph->Connect(teeOutPre, renIn);

#ifdef DX_WINDOWLESS
	if (m_hwnd)
		InitializeWindowlessVMR(m_hwnd, m_render);
#endif

	updateWindow();
	
	hr = getMediaControl()->Run();
	
	return S_OK;
}

//
HRESULT TDXGraph::updateWindow()
{
#ifndef DX_WINDOWLESS
	return S_OK;
#endif
	HRESULT hr;
	CComPtr<IVMRWindowlessControl9> pwc;
	hr = m_render->QueryInterface(IID_IVMRWindowlessControl9, (void**)&pwc);
	if (!SUCCEEDED(hr))
		return hr;

	long lWidth, lHeight;
	hr = pwc->GetNativeVideoSize(&lWidth, &lHeight, NULL, NULL);
	if (SUCCEEDED(hr))
	{
		RECT rcSrc, rcDest;
		// Set the source rectangle.
		SetRect(&rcSrc, 0, 0, lWidth, lHeight);

		// Get the window client area.
		GetClientRect(m_hwnd, &rcDest);
		// Set the destination rectangle.
		SetRect(&rcDest, 0, 0, rcDest.right, rcDest.bottom);

		// Set the video position.
		hr = pwc->SetVideoPosition(&rcSrc, &rcDest);
	}
//	HDC hdc = GetDC(m_hwnd);
//	pwc->RepaintVideo(m_hwnd,hdc);

	return hr;
}
//
void TDXGraph::beginRecord(const QString &path)
{
	HRESULT hr;
	OAFilterState fs;
	
	m_vp = path;
		
	hr = getMediaControl()->Stop();
	
	hr = getMediaControl()->GetState(10000, &fs);
	if (!SUCCEEDED(hr))
		return;

	hr = buildGraph();

//	prepareFileOutput();


	if (fs == State_Stopped){
		updateWindow();
		/**
		CComPtr<IPin> teeOut, matroskaIn;
		m_teeCapOut = GetPin(m_tee, PINDIR_OUTPUT);
		hr = m_graph->Connect(m_teeCapOut, m_matroskaIn);
		CComPtr<IPin> matroskaout, filein;
		matroskaout = GetPin(m_matroska, PINDIR_OUTPUT);
		hr = SetFileName(matroskaout, m_file, m_vp);
		filein = GetPin(m_file, PINDIR_INPUT);
		hr = m_graph->Connect(matroskaout, filein);
		/**/
//		CComPtr<IPin> teeOut, matroskaIn;
//		m_teeCapOut = GetPin(m_tee, PINDIR_OUTPUT);
//		hr = m_graph->Connect(m_teeCapOut, m_matroskaIn);
		CComPtr<IPin> matroskaout, filein;
		matroskaout = GetPin(m_matroska, PINDIR_OUTPUT);
		hr = SetFileName(matroskaout, m_file, m_vp);
		filein = GetPin(m_file, PINDIR_INPUT);
		hr = m_graph->Connect(matroskaout, filein);


	}
	else
		qDebug() << "start::Can't stop";

	hr = getMediaControl()->Run();

	//!!!m_isPrepared = true;
}

//
void TDXGraph::endRecord()
{
	HRESULT hr;
	OAFilterState fs;

	hr = getMediaControl()->Stop();

	hr = getMediaControl()->GetState(10000, &fs);
	if (!SUCCEEDED(hr))
		return;

	if (fs == State_Stopped){
/**		CComPtr<IPin> teeOut, matroskaIn;
		hr = m_teeCapOut->Disconnect();
		m_teeCapOut.Release();
		hr = m_matroskaIn->Disconnect();
		/**/		
		CComPtr<IPin> matroskaout, filein;
		matroskaout = GetPin(m_matroska, PINDIR_OUTPUT,false);
		filein = GetPin(m_file, PINDIR_INPUT,false);
		hr = matroskaout->Disconnect();
		hr = filein->Disconnect();
	}
	else
		qDebug() << "stop::Can't stop";

	hr = getMediaControl()->Run();

	//!!!!m_isPrepared = false;
}

//
void TDXGraph::takeSnapshot(const QString &path)
{
	HRESULT hr;
	CComPtr<IVMRWindowlessControl9> pwc;
	hr = m_render->QueryInterface(IID_IVMRWindowlessControl9, (void**)&pwc);
	if (!SUCCEEDED(hr))
		return;
	
	BYTE *dib;
	hr = pwc->GetCurrentImage(&dib);
	if (!SUCCEEDED(hr))
		return;

	BITMAPINFOHEADER  *pBMIH = (BITMAPINFOHEADER*)dib;

	QImage im(dib + sizeof(BITMAPINFOHEADER), pBMIH->biWidth, pBMIH->biHeight, QImage::Format_ARGB32);
	im = im.mirrored(false, true);

	HANDLE fl = ::CreateFileA(path.toLatin1().constData(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	QByteArray ba; 
	QBuffer buff(&ba);
	buff.open(QIODevice::WriteOnly);
	im.save(&buff, "JPG");

	DWORD wrt;
	BOOL res = ::WriteFile(fl, buff.data().constData(), buff.size(), &wrt, NULL);
	if (!res)
		qDebug() << "snapshot:WriteFile failed.";
	
	::CloseHandle(fl);
	::CoTaskMemFree(dib);
}

#endif //USE_DIRECTX_CAPTURE